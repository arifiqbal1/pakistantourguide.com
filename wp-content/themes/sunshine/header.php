<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!--Fav Icon-->
  <link href="<?= IMAGES ?>/favicon.png" type="image/x-icon" rel="shortcut icon"/>
  <link href="<?= IMAGES ?>/favicon.png" type="image/x-icon" rel="icon"/>

  <?php wp_head(); ?>

  <?php header_tracking_scripts() ?>

</head>

<body <?php body_class(); ?>>

<header class="tif-header tif-header-transparent">
  <div class="container">
    <?php get_main_nav() ?>
  </div>
</header>
