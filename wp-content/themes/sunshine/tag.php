<?php
  get_header('without-hero');
?>

<?php get_cats_menu(); ?>

<?php if ( function_exists('yoast_breadcrumb') ) : ?>
<section class="breadcrumb-section">
  <div class="container">
    <?php yoast_breadcrumb('<p id="breadcrumbs">','</p>') ?>
  </div>
</section>
<?php endif; ?>

<section class="blog-section">
  <div class="container">
    <h1><?php single_tag_title() ?></h1>
    <?php get_template_part('templates/blog-loop') ?>
  </div>
</section>

<?php get_footer(); ?>
