<?php get_header(); ?>

<?php
  $term_id = $wp_query->get_queried_object_id();
  $activity = new Activity($term_id);

  $attractions = Attraction::find_attractions(0, $term_id);
  $showReviews = false;
?>

<section class="hero-section" style="background-image: url(<?php echo $activity->get_image() ?>)">
	<div class="container">
		<div class="hero-content">
			<div class="hero-content-outer">
				<div class="hero-content-inner">
					<h1><?= $activity->get_name() ?> in Florida</h1>
          <?php if($activity->get_description()): ?>
					  <p><?php echo $activity->get_description() ?></p>
          <?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="breadcrumb-section">
  <div class="container">
    <ol itemscope itemtype="http://schema.org/BreadcrumbList" class="clearfix">
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a href="<?= URL ?>" itemprop="item">
          <span itemprop="name">Home</span>
        </a>
      </li>
      <li>
        <span><?= $activity->get_name() ?></span>
      </li>
    </ol>
  </div>
</section>

<?php //get_attraction_search() ?>

<section class="attractions-search-results tif-section">
  <div class="container">
    <div class="city-attractions-cols clearfix">
      <?php foreach($attractions as $attraction): ?>
        <?php attraction_grid_item($attraction) ?>
      <?php endforeach; ?>
    </div>
  </div>
</section>

<?php
$blog_query = new WP_Query( array( 
        'post_type' => 'post',
        'posts_per_page' => -1, 
        'tax_query' => array(
            array(
              'taxonomy' => 'activities',
              'field'    => 'term_id',
              'terms'    => $term_id,
            ),
          ),
      )); 
?>
<?php if ($blog_query->have_posts()): ?>
<section class="tif-section featured-blog-section">
  <div class="container">
    <h2>Articles about <?= $activity->get_name() ?> in Florida</h2>

    <div class="featured-blog-cols clearfix">
      <?php while($blog_query->have_posts()): $blog_query->the_post(); ?>
      <?php blog_grid_item() ?>
      <?php endwhile; ?>
    </div>
  </div>
</section>
<?php endif; ?>
<?php wp_reset_postdata(); ?>

<?php get_footer(); ?>
