<?php

//To Refresh Google Place Cache
$refreshGP = false;
if(isset($_GET['refreshGP'])) $refreshGP = true;

get_header('without-hero') ?>

<?php

$dayToday     = current_time("l"); //get the dat e.g. Saturday
$currnt_day   = strtolower($dayToday);
$open_check   = get_field($currnt_day);
$website_url  = get_field('website_url');
$url_parse    = parse_url($website_url);
$website_domain = $url_parse['host'];

// check if the flexible content field has rows of data
if($open_check) {
  $start_time = get_field($currnt_day.'_from');
  $close_time = get_field($currnt_day.'_to');
  $timing     = $start_time.' - '.$close_time;
}
else {
  $timing = 'Closed';
}

$field 		  = get_sub_field_object($currnt_day.'_open');
$value 		  = get_sub_field($currnt_day.'_open');
$label 		  = $field['choices'][ $value ];
$check_open = ucfirst($label);

// Start the Main Loop.
while ( have_posts() ) : the_post();

if(!$address || $address == "") $address = false;

$attraction = new Attraction(get_the_ID());

//Refresh the Google Place Object
if($refreshGP) $attraction->refresh_GooglePlaceObject();

$google_map_data = array(get_the_title(), $attraction->get_address(),$attraction->get_city(),$attraction->get_state());

$city = get_field('city');
?>

<section id="tourist-attraction" class="tourist-attraction" itemscope itemhero-contenttype="http://schema.org/TouristAttraction" data-lng="<?= $attraction->get_longitude() ?>" data-lat="<?= $attraction->get_latitude() ?>">
	
	<section class="breadcrumb-section">
		<div class="container">
			<ol itemscope itemtype="http://schema.org/BreadcrumbList" class="clearfix">
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a href="<?= URL ?>" itemprop="item">
						<span itemprop="name">Home</span>
					</a>
				</li>
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a href="<?= URL ?>/things-to-do/<?= $city->post_name ?>/" itemprop="item">
						<span itemprop="name">Things to do in <?php echo get_the_title(get_field('city'));?></span>
					</a>
				</li>
				<li>
					<span><?= get_the_title();?></span>
				</li>
			</ol>
		</div>
	</section>

	<section class="attraction-details">
		<div class="container clearfix">
			
			<div class="attractions-left">
				<div id="attraction-nav" class="attraction-nav">
					<ul>
						<li><a href="#attraction-overview">Overview</a></li>

						<?php if (has_post_thumbnail()) : ?>
						<li><a href="#attraction-photos">Photos</a></li>
						<?php endif; ?>

						<?php if(0): //$attraction->get_timings() // 0 to disable this section?>
						<li><a href="#attraction-hours">Hours</a></li>
						<?php endif; ?>
						
						<?php if($attraction->get_address()): ?>
						<li><a href="#attraction-directions">Directions & Map</a></li>
						<?php else: ?>
						<li><a href="#attraction-directions">Map</a></li>
						<?php endif; ?>

						<?php if( have_rows('faq') ): ?>
						<li><a href="#attraction-faq">FAQ</a></li>
						<?php endif; ?>

						<?php if( '' !== get_post()->post_content || $attraction->get_wiki_overview() ): ?>
						<li><a href="#attraction-description">Description</a></li>
						<?php endif; ?>
					</ul>
				</div>

				<?php if(SHOWADS): ?>
				<div class="attraction-left-ad">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- attraction-sidebar -->
					<ins class="adsbygoogle"
					     style="display:block"
					     data-ad-client="ca-pub-4605913240682708"
					     data-ad-slot="1208292671"
					     data-ad-format="auto"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
				<?php endif; ?>

			</div>

			<div class="attractions-right">
				<div id="attraction-overview">
					<h1 itemprop="name"><?php echo get_the_title();?></h1>
					<p class="attraction-intro" itemprop="description">
						<?php echo get_field('short_description');?>
					</p>

					<?php if($attraction->get_phone()!= null || $attraction->get_website()!=null || $attraction->get_address() != null): ?>
					<div class="overview-info clearfix">
						<?php if($attraction->get_address() && $attraction->get_address() != ""): ?>
						<div class="attraction-address clearfix">
		  					<span class="address-icon icon-location"></span>
		  					<span class="attraction-address-text" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<?php
									$line_break = "<br />";
									if(strlen($address) > 23):
										$line_break = "";
									endif;
								?>
		  						<span itemprop="streetAddress"><?= $attraction->get_address() ?></span>, <?php echo $line_break ?>
		  					    <span itemprop="addressLocality"><?= $attraction->get_city() ?></span>,
		  					    <span itemprop="addressRegion"><?= $attraction->get_state() ?></span>
		  					    <span itemprop="postalCode"><?= $attraction->get_zip() ?></span>
		  					</span>
		  				</div>
		  				<?php endif; ?>
		  				
		  				<div class="attraction-other-details">
		  					<?php if($attraction->get_phone() && $attraction->get_phone() != ""): ?>
		  					<div class="attraction-phone">
			  					<span class="attraction-phone-icon icon-phone"></span>
			  					<span class="attraction-phone-number"><?= $attraction->get_phone() ?></span>
			  				</div>
			  				<?php endif; ?>

			  				<?php if($attraction->get_website() && $attraction->get_website() != ""): ?>
			  				<div class="attraction-website">
			  					<span class="attraction-website-icon icon-link"></span>
			  					<span class="attraction-website-number"><?= get_url_domain($attraction->get_website()) ?></span>
			  				</div>
			  				<?php endif; ?>
		  				</div>
					</div>
					<?php endif; ?>
				</div>

				<?php if (has_post_thumbnail()) : ?>
<!--					<div id="attraction-photos" class="attractions-infobox attraction-pictures">
						<div class="infobox-content">
							<div class="featured-img">
								<?php //the_post_thumbnail(); ?>
							</div>
						</div>
					</div>-->
                                        <?php if( have_rows('attraction_gallery') ): ?>
                                            <div id="slider" class="flexslider">
                                                <ul class="slides">
                                                <?php while( have_rows('attraction_gallery') ): the_row(); 

                                                            // vars
                                                            $image = get_sub_field('image');
//                                                            echo '<pre>'.print_r($image,TRUE).'</pre>';break;die();

                                                            ?>
                                                    <li>
                                                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                                                    </li>
                                                <?php endwhile; ?>
                                                </ul>
                                            </div>
                                            <div id="slider-thumbs" class="flexslider">
                                                <ul class="slides">
                                                    <?php while( have_rows('attraction_gallery') ): the_row(); 
                                                                $image = get_sub_field('image');
    //                                                            echo '<pre>'.print_r($image,TRUE).'</pre>';break;die();
                                                                ?>
                                                                <li>
                                                                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt'] ?>" />
                                                                </li>
                                                    <?php endwhile; ?>
                                                </ul>
                                            </div>
                                        <?php endif; ?>

					<?php if(get_field('photo_credit')): ?>
					<div class="photo-credit"><?= get_field('photo_credit') ?></div>
					<?php endif; ?>

				<?php endif; ?>
                                        
				<?php if(0): //$attraction->get_timings() //0 to disable this section ?>
				<div id="attraction-hours" class="attractions-infobox">

					<div class="infobox-header clearfix">
						<h2>Hours</h2>
						<?php if($attraction->get_timings()->open_now): ?>
						<div class="info-header-right">
							<span class="open-today"><span class="timing-icon icon-ok"></span> Open Now</span>
						</div>
						<?php endif; ?>
					</div>

					<div class="infobox-content">
						<?php if(count($attraction->get_timings()->periods) < 7): ?>

							<div class="hours-list-div">
								<ul>
								<?php foreach($attraction->get_timings()->weekday_text as $timing): ?>
									<li><?= $timing ?></li>
								<?php endforeach; ?>
								</ul>
							</div>

						<?php else: ?>
							<ul class="hours-list">
								<li class="clearfix hours-list-head">
									<span class="dayname">Day</span>
									<span class="opentime">Open</span>
									<span class="closetime">Close</span>
								</li>

								<?php $dayCount = 0 ?>
								<?php foreach($attraction->get_timings()->periods as $timing): ?>
								<li class="clearfix">
									<span class="dayname"><?= get_day_name($dayCount) ?></span>
									<span class="opentime"><?= date("g:i A", strtotime($timing->open->time)) ?></span>
									<span class="closetime"><?= date("g:i A", strtotime($timing->close->time)) ?></span>
								</li>
								<?php $dayCount++ ?>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
				<?php endif; ?>

				<?php if($attraction->get_address()): ?>
				<div id="attraction-directions" class="attractions-infobox">
					<div class="infobox-header clearfix">
						<h2>Directions & Map</h2>
						<div class="info-header-right">
							<a href="https://maps.google.com?saddr=Current+Location&daddr=<?php echo implode("+", $google_map_data); ?>" rel="nofollow" target="_blank" class="btn-green">Get Directions <span class="icon-right-open"></span></a>
						</div>
					</div>
					<div class="infobox-content">
						<div id="attraction-map" class="attraction-map"></div>
					</div>
				</div>
				<?php else: ?>
				<div id="attraction-directions" class="attractions-infobox">
					<div class="infobox-header clearfix">
						<h2>Map</h2>
					</div>
					<div class="infobox-content">
						<div id="attraction-map" class="attraction-map"></div>
					</div>
				</div>
				<?php endif; ?>

				<?php if( have_rows('faq') ): ?>
				<div id="attraction-faq" class="attractions-infobox">
					<div class="infobox-header clearfix">
						<h2>Frequently Asked Questions</h2>
						<!-- <div class="info-header-right"></div> -->
					</div>

					<div class="infobox-content attraction-text">
						<?php while( have_rows('faq') ): the_row(); ?>
						<div class="faq-question-item">
							<h3 class="faq-question"><?php echo get_sub_field('faq_question'); ?></h3>
							<div class="faq-answer">
								<?php echo apply_filters( 'the_content',get_sub_field('faq_answer',false, false));?>
							</div>
						</div>
						<?php endwhile; ?>
					</div>
				</div>
				<?php endif; ?>

				<?php if( '' !== get_post()->post_content || $attraction->get_wiki_overview() != ""): ?>
				<div id="attraction-description" class="attractions-infobox">
					<div class="infobox-header clearfix">
						<h2>Description</h2>
						<!-- <div class="info-header-right"></div> -->
					</div>
					<div class="infobox-content attraction-text">
					<?php 
						if( '' !== get_post()->post_content) {
							the_content();
						}
						else {
							echo $attraction->get_wiki_overview();
						}  
					?>
					</div>
				</div>
				<?php endif; ?>

				<?php
				$blog_query = new WP_Query( array( 
								'post_type' => 'post',
								'posts_per_page' => -1, 
								'meta_query' => array(
							        array(
							            'key' => 'article_relations', // name of custom field
							            'value' => '"' . get_the_ID() . '"',
							            'compare' => 'LIKE'
							        )
							    )
							)); 
				?>
				<?php if ($blog_query->have_posts()): ?>
				<div id="attraction-articles" class="attractions-infobox featured-blog-section">
					<div class="infobox-header clearfix">
						<h2 class="full-width-h2">Articles About <span><?= get_the_title();?></span></h2>
					</div>
					<div class="infobox-content attraction-text">
						<div class="featured-blog-cols clearfix">
							<?php while($blog_query->have_posts()): $blog_query->the_post(); ?>
							<?php blog_grid_item() ?>
							<?php endwhile; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>

			</div>
		</div>
	</section>

</section>

<?php
// End of the Main Loop.
endwhile;
get_footer(); ?>