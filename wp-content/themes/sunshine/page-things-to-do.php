<?php

get_header();
while ( have_posts() ) : the_post();
?>

<section class="hero-section" style="background-image: url(<?php echo get_featured_image(get_the_ID()) ?>)">
  <div class="container">
    <div class="hero-content">
      <div class="hero-content-outer">
        <div class="hero-content-inner">
          <h1 itemprop="name"><?php the_title() ?></h1>
          <p itemprop="description"><?php the_field('short_description') ?></p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="breadcrumb-section">
  <div class="container">
    <ol itemscope itemtype="http://schema.org/BreadcrumbList" class="clearfix">
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a href="<?= URL ?>" itemprop="item">
          <span itemprop="name">Home</span>
        </a>
      </li>
      <li>
        <span>Things to do in Florida</span>
      </li>
    </ol>
  </div>
</section>

<?php
  $attractions = Attraction::find_attractions(0,0,12,'rand');
?>
<?php if(count($attractions) > 0): ?>
<section class="tif-section">
  <div class="container">
    <h2>Tourist Attractions in Florida</h2>

    <div class="city-attractions-cols clearfix">
      <?php foreach($attractions as $attraction): ?>
        <?php attraction_grid_item($attraction) ?>
      <?php endforeach; ?>
    </div>

    <div class="tif-section-cta">
      <a href="<?php echo URL ?>/attractions/">View more attractions</a>
    </div>
  </div>
</section>
<?php endif; ?>

<?php
global $post;
$page_slug=$post->post_name;
$blog_query = new WP_Query( array( 'posts_per_page' => 16, 'category_name' => $page_slug ) ); ?>
<?php if ($blog_query->have_posts()): ?>
<section class="featured-blog-section tif-section">
  <div class="container">
    <h2>Articles about Things to do in Florida</h2>

    <div class="featured-blog-cols clearfix">
      <?php while($blog_query->have_posts()): $blog_query->the_post(); ?>
      <div class="featured-blog-col">
        <div class="featured-blog">
          <div class="featured-blog-img img-container">
            <?php if (has_post_thumbnail()) : ?>
                    <?php the_post_thumbnail('blogpost-thumb'); ?>
                  <?php endif; ?>
          </div>
          <div class="featured-blog-date">
            <p>Published on <?= get_the_date('F j') ?></p>
          </div>
          <h3 class="featured-blog-name">
            <a href="<?= get_permalink() ?>"><?= get_the_title() ?></a>
          </h3>
          <div class="featured-blog-description">
            <p><?php the_field('short_description') ?></p>
          </div>
          <div class="featured-blog-more-info">
            <a href="<?= get_permalink() ?>">View Post</a>
          </div>
        </div>
      </div>
      <?php endwhile; ?>
    </div>

    <div class="load-more-blogs">
      <a href="<?= URL ?>/blog/">View more articles</a>
    </div>
  </div>
</section>
<?php endif; ?>
<?php wp_reset_postdata(); ?>

<?php $cities = City::get_all_cities(); ?>
<section class="tif-section tif-blue-bg cities-section">
  <div class="container">
    <h2>Find things to do in Florida by cities</h2>

    <ul class="clearfix">
      <?php foreach($cities as $city): ?>
      <li>
        <a href="<?php echo $city->get_url() ?>">
          <?php echo $city->get_name() ?>
        </a>
      </li>
      <?php endforeach; ?>
    </ul>
  </div>
</section>

<section class="post-content tif-section">
  <div class="container">
    <h2>Best of Florida: What to See and Do at the Sunshine State</h2>
    <div class="textcontent-section">
      <?php the_content() ?>
    </div>
  </div>
</section>


<?php endwhile; ?>
<?php get_footer(); ?>
