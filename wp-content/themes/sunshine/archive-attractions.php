<?php
  /* Template Name: Attractions Search */
  get_header();
?>

<?php

  $args       = array();
  $cityID     = intval(get_query_var('city'));
  $activityID = intval(get_query_var('activity'));

  global $wp_query;

  if($cityID || $activityID) {
    $args = Attraction::find_attractions_query($cityID, $activityID);
  	$args = array_merge( $wp_query->query_vars, $args );
    query_posts( $args );
  }

  $totalAttractions = $wp_query->found_posts;
  $pluralResults = "s";
  if($totalAttractions == 1) $pluralResults = "";

  $heading = "We have found {$totalAttractions} attraction{$pluralResults} for you..!";

  if($totalAttractions <= 0) {
    $heading = "Looks like we couldn't find anything for your query..!";
  }

  $heroClass = "search-hero";

  if( !$cityID && !$activityID ) {
    $heading = "Attractions in Florida";
    $heroClass = "search-hero-normal";
  }

  $showReviews = false;
?>
<section class="hero-section <?php echo $heroClass ?>">
	<div class="container">
		<div class="hero-content">
			<div class="hero-content-outer">
				<div class="hero-content-inner">
					<h1><?php echo $heading ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="breadcrumb-section">
  <div class="container">
    <ol itemscope itemtype="http://schema.org/BreadcrumbList" class="clearfix">
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a href="<?= URL ?>" itemprop="item">
          <span itemprop="name">Home</span>
        </a>
      </li>
      <li>
        <span>Attractions in Florida</span>
      </li>
    </ol>
  </div>
</section>

<?php get_attraction_search($cityID, $activityID) ?>

<?php if(have_posts()): ?>
<section class="attractions-search-results tif-section">
  <div class="container">
    <div class="city-attractions-cols clearfix">
      <?php while ( have_posts() ): the_post(); ?>
      <?php
        $attraction = new Attraction(get_the_ID());
        attraction_grid_item($attraction);
      ?>
      <?php endwhile; ?>
    </div>

    <div class="attraction-pagination">
      <?php
        $paginate_arg = array(
          // 'prev_text' => __('« Previous'),
	        // 'next_text' => __('Next »'),
        );
        echo paginate_links( $paginate_args );
      ?>
    </div>

  </div>
</section>
<?php endif; ?>


<?php
$blog_query = new WP_Query( array( 
        'post_type' => 'post',
        'posts_per_page' => -1, 
        'tax_query' => array(
            array(
              'taxonomy' => 'activities',
              'field'    => 'slug',
              'terms'    => 'bob',
            ),
          ),
      )); 
?>
<?php if ($blog_query->have_posts()): ?>
<section class="tif-section featured-blog-section">
  <div class="container">
    <h2>Articles about <?= $city->get_name() ?></h2>

    <div class="featured-blog-cols clearfix">
      <?php while($blog_query->have_posts()): $blog_query->the_post(); ?>
      <?php blog_grid_item() ?>
      <?php endwhile; ?>
    </div>
  </div>
</section>
<?php endif; ?>
<?php wp_reset_postdata(); ?>

<?php get_footer(); ?>
