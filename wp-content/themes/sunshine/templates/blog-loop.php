<?php
if(have_posts()):
  $colorArray = array('lightblue', 'red', 'blue', 'green', 'orange'); ?>

<div class="blogpost-items-container">
  <div class="blogpost-items clearfix">
    <?php
    while ( have_posts() ) : the_post();
    $color = $colorArray[array_rand($colorArray)];
    ?>
    <div class="blogpost-item blog-<?php echo $color ?>-color">
      <div class="blogpost-item-inner">
        <div class="blogpost-img">
          <a href="<?php the_permalink(); ?>">
            <?php if (has_post_thumbnail()) : ?>
              <?php the_post_thumbnail('blogpost-thumb'); ?>
            <?php endif; ?>
          </a>
        </div>
        <h3><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>
        <p><?php the_field('short_description') ?></p>
      </div>
    </div>
    <?php endwhile; ?>
  </div>
  <div class="blog-pagination">

    <?php
      $paginate_args = array(
        // 'prev_text' => __('«'),
        // 'next_text' => __('»'),
        'prev_text' => __('<span class="icon-left-open"></span>'),
        'next_text' => __('<span class="icon-right-open"></span>'),
      );
      echo paginate_links( $paginate_args )
    ?>
  </div>
</div>
<?php endif; ?>
