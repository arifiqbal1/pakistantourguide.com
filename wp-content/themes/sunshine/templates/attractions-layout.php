<?php get_header();

$dayToday     = current_time("l"); //get the dat e.g. Saturday
$currnt_day   = strtolower($dayToday);
$open_check   = get_field($currnt_day);
$website_url  = get_field('website_url');
$url_parse    = parse_url($website_url);
$website_domain = $url_parse['host'];

// check if the flexible content field has rows of data
if($open_check) {
  $start_time = get_field($currnt_day.'_from');
  $close_time = get_field($currnt_day.'_to');
  $timing     = $start_time.' - '.$close_time;
}
else {
  $timing = 'Closed';
}

$field 		  = get_sub_field_object($currnt_day.'_open');
$value 		  = get_sub_field($currnt_day.'_open');
$label 		  = $field['choices'][ $value ];
$check_open = ucfirst($label);

$address 			= get_field('address');
$city 				= get_field('city');

$state 				= 'FL';
$zip 				  = get_field('zip');
$google_map_data 	= array($address,$city->post_title,$state,$zip);

// Start the Main Loop.
while ( have_posts() ) : the_post();


if(!$address || $address == "") $address = false;

$attraction = new Attraction(get_the_ID());

?>
<section class="tourist-attraction" itemscope itemhero-contenttype="http://schema.org/TouristAttraction">
	<section class="hero-section" style="background-image: url(<?= get_the_post_thumbnail_url();?>)">
		<div class="container">
			<div class="hero-content <?= (!$address)? "hero-content-full" : "" ?>">
				<div class="hero-content-outer">
					<div class="hero-content-inner">
						<h1 itemprop="name"><?php echo get_the_title();?></h1>
						<p itemprop="description">
							<?php echo get_field('short_description');?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="breadcrumb-section">
		<div class="container">
			<ol itemscope itemtype="http://schema.org/BreadcrumbList" class="clearfix">
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a href="<?= URL ?>" itemprop="item">
						<span itemprop="name">Home</span>
					</a>
				</li>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a href="<?= URL ?>/things-to-do/" itemprop="item">
						<span itemprop="name">Things to do in Florida</span>
					</a>
				</li>
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a href="<?= URL ?>/things-to-do/<?= $city->post_name ?>/" itemprop="item">
						<span itemprop="name"><?php echo get_the_title(get_field('city'));?></span>
					</a>
				</li>
				<li>
					<span><?php echo get_the_title();?></span>
				</li>
			</ol>
		</div>
	</section>

  <?php if($address): ?>
	<section class="attraction-info-widget">
		<div class="container">
			<div class="attraction-widget">
        <div class="attraction-widget-top">
          <div class="weather-report hot clearfix">
  					<div class="weather-report-wait">
  						<span class="icon-spin animate-spin"></span>
  						<p>Getting Current Weather!</p>
  					</div>
  					<div class="weather-report-data" style="display:none">
  						<span class="current-weather-icon">
  							<img src="<?= IMAGES ?>/icons/weather/sunny.png" alt="Sunny">
  						</span>
  						<span class="current-temprature">0</span>
  						<!--span class="current-temprature temp-celsius">0</span-->
  						<span class="temprature-units">&deg;F</span>
  						<!--span class="temprature-units temp-celsius">&deg;C</span-->
  					</div>
  				</div>
  				<?php
  					$place_id = $attraction->get_GooglePlaceID();
  					if($place_id):
  						//This code is taking too much time but how can we optimize
              $timing = $attraction->get_timings($place_id);
	         		if($timing):
                $Today  = date('N', time());
                $todayTime = $timing->weekday_text[$Today-1]; // minus 1 mean it will be 0 for monday otherwise 1 for monday
                $arr = explode(':', $todayTime,2);
                $justTime = $arr[1];// return 10:00am - 9:00pm
                $periods  = $timing->periods;
                $OpenToday = false;
                  if($periods){
                    foreach ($periods as $key => $value) {
                      if($Today == 7){
                        $Today = 0; // it is sunday  0
                      }
                      if($value->open->day == $Today){
                        $OpenToday = true;
                      }
                    }
                }
		        	?>
	    				<div class="timing-status clearfix">
	    				<?php if( $timing->open_now || $OpenToday == true): ?>
          			<span class="open-today">
    							<span class="timing-icon icon-ok"></span> Open Today
    						</span>
    						<span class="timing-bullet">&bull;</span>
    						<span class="open-timings"><?php echo $justTime?></span>
	    				<?php else: ?>
         		 		<span class="close-today">
    							<span class="timing-icon icon-cancel"></span> Closed Today
    						</span>
	    				<?php endif; ?>
	    				</div>
    				<?php endif;  ?>
    			<?php endif;  ?>
  				<div class="attraction-address clearfix">
  					<span class="address-icon icon-location"></span>
  					<span class="attraction-address-text" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
              <?php
                $line_break = "<br />";
                if(strlen($address) > 23):
                  $line_break = "";
                endif;
              ?>
  						<span itemprop="streetAddress"><?php echo $address?></span>, <?php echo $line_break ?>
  					    <span itemprop="addressLocality"><?php echo $city->post_title; ?></span>,
  					    <span itemprop="addressRegion"><?php echo $state; ?></span>
  					    <span itemprop="postalCode"><?php echo $zip; ?></span>
  					</span>
  				</div>
  				<div class="attraction-get-directions">
  					<a href="https://maps.google.com?saddr=Current+Location&daddr=<?php echo implode("+", $google_map_data); ?>" rel="nofollow" target="_blank">Get Directions <span class="icon-right-open"></span></a>
  				</div>

          <?php if(get_field('phone_number')): ?>
  				<div class="attraction-phone-number">
            <span class="icon-phone"></span>
            <?php echo "(".substr(get_field('phone_number'),0,3).") ".substr(get_field('phone_number'),3,3)."-".substr(get_field('phone_number'),6,30);?>
          </div>
          <?php endif; ?>

        </div>
				<div class="attraction-widget-devider"></div>

        <?php if(get_field('website_url') || get_field('buy_tickets_url')): ?>
  				<div class="attraction-actions">
            <?php if(get_field('website_url')): ?>
              <a href="<?php echo $website_url;?>" class="attraction-visit-website" target="_blank" rel="nofollow">Visit website</a>
            <?php endif; ?>

            <?php if(get_field('buy_tickets_url')): ?>
              <a href="<?= get_field('buy_tickets_url') ?>" class="attraction-buy-tickets" target="_blank" rel="nofollow">Buy tickets</a>
            <?php endif; ?>
  				</div>
        <?php endif; ?>
			</div>
		</div>
	</section>
  <?php endif; ?>

  <section class="attraction-tabs">
		<div class="container">
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Overview</a></li>
				<!-- <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Reviews</a></li> -->
				<!-- <li role="presentation"><a href="#faqs" aria-controls="faqs" role="tab" data-toggle="tab">FAQ</a></li> -->
        <li role="presentation"><a href="#faqs" aria-controls="faqs" role="tab" data-toggle="tab">FAQ</a></li>
				<!--li role="presentation"><a href="#rides" aria-controls="rides" role="tab" data-toggle="tab">Rides</a></li-->
			</ul>
		</div>
	</section>

	<section class="tab-content">
		<div role="presentation" class="tab-expand-mobile">
			<a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Overview</a>
		</div>
		<div role="tabpanel" class="tab-pane active" id="overview">
			<section class="overview-tab tif-section">
				<div class="container">
					<h2>About <strong><?php the_title() ?></strong></h2>
					<div class="post-content">
						<?php the_content(); ?>
					</div>
				</div>
			</section>
		</div>

		<!-- <div role="presentation" class="tab-expand-mobile">
			<a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Reviews</a>
		</div> -->

    <!--
		<div role="tabpanel" class="tab-pane" id="reviews">
			<section class="review-social tif-section">
				<div class="container clearfix">
					<div class="review-section">
					 	<div class="review-head clearfix">
					 		<h2>Reviews</h2>
					 		<span class="review-score">4.6</span>
					 		<span class="rating">
								<span class="star rating-6"></span>
								<span class="star rating-7"></span>
								<span class="star rating-8"></span>
								<span class="star rating-9"></span>
								<span class="star rating-10"></span>
							</span>
							<a href="#" class="total-reviews">564 reviews</a>
							<a href="#" class="write-review">Write a review</a>
					 	</div>
						<ul id="owl-review" class="reviews owl-carousel">
							<li class="item" >
								<div class="review-author meta">
									<h3>Clark Wayne</h3>
									<span>Industries W</span>
								</div>
								<div class="review-para">
									<p>
										It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
									</p>
								</div>
								<span class="rating">
									<span class="star rating-6"></span>
									<span class="star rating-7"></span>
									<span class="star rating-8"></span>
									<span class="star rating-9"></span>
									<span class="star rating-10"></span>
								</span>
							</li>
							<li class="item" >
								<div class="review-author meta">
									<h3>Clark Wayne</h3>
									<span>Industries W</span>
								</div>
								<div class="review-para">
									<p>
										It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
									</p>
								</div>
								<span class="rating">
									<span class="star rating-6"></span>
									<span class="star rating-7"></span>
									<span class="star rating-8"></span>
									<span class="star rating-9"></span>
									<span class="star rating-10"></span>
								</span>
							</li>

						</ul>
						<div class="owl-controls clearfix">
							<div class="next">
								<span class="demo-icon icon-right"></span>
							</div>
							<div class="prev">
								<span class="demo-icon icon-left"></span>
							</div>
						</div>
					</div>

					<div class="social-widget">
						<h3>Follow Us</h3>
						<ul>
							<li>
								<a href="#">
									<i class="icon-facebook"></i>
									<span>Facebook</span>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="icon-twitter"></i>
									<span>Twitter</span>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="icon-youtube-play"></i>
									<span>youtube</span>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="icon-instagram"></i>
									<span>instagram</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>
		</div>
    -->

		<div role="presentation" class="tab-expand-mobile">
      <a href="#faqs" aria-controls="faqs" role="tab" data-toggle="tab">FAQs</a>
		</div>
		<div role="tabpanel" class="tab-pane" id="faqs">
			<section class="faq tif-section">
				<div class="container">
					<div class="inner-faq">
            <h2>Frequently Asked Questions</h2>
						<ul>
							<!-- Getting Faq Questions -->
						<?php if( have_rows('faq') ): ?>
							<?php while( have_rows('faq') ): the_row(); ?>

										<li>
											<a class="faq-heading" href="#">
												<h3><?php echo get_sub_field('faq_question'); ?></h3>
												<i class="demo-icon icon-down-open"></i>
												<i class="demo-icon icon-up-open"></i>
											</a>
											<div class="faq-para">
												<?php echo apply_filters( 'the_content',get_sub_field('faq_answer',false, false));?>
											</div>
										</li>

							<?php endwhile; ?>
						<?php endif; ?>
						</ul>
					</div>
				</div>
			</section>
		</div>

		<!--div role="presentation" class="tab-expand-mobile">
			<a href="#rides" aria-controls="rides" role="tab" data-toggle="tab">Rides</a>
		</div>
		<div role="tabpanel" class="tab-pane" id="rides">
			<section class="attraction-rides tif-section tif-blue-bg">
				<div class="container">
					<h2>Rides</h2>
					<div class="attraction-rides-cols clearfix">
						<div class="attraction-rides-col">
							<div class="attraction-ride">
								<div class="attraction-ride-img">
									<a href="#">
										<img src="<?= IMAGES ?>/attraction.jpg" alt="">
									</a>
								</div>
								<h3 class="attraction-ride-name">
									<a href="#">Sea Trek Reef Encounter</a>
								</h3>
								<div class="attraction-ride-description">
									Sea Trek Reef Encounter is an underwater walking journey through our 300,000 gallon tropical reef. While wearing a state-of-the-art dive helmet that allows you to breathe freely, you'll feel right at home under the sea. During your 20-minute journey, you'll encounter tropical fish, sting rays and a variety of unique sea creatures.
								</div>
								<div class="attraction-ride-more-info">
									<a href="#">More info</a>
								</div>
							</div>
						</div>
						<div class="attraction-rides-col">
							<div class="attraction-ride">
								<div class="attraction-ride-img">
									<a href="#">
										<img src="<?= IMAGES ?>/attraction.jpg" alt="">
									</a>
								</div>
								<h3 class="attraction-ride-name">
									<a href="#">Sea Trek Reef Encounter</a>
								</h3>
								<div class="attraction-ride-description">
									Sea Trek Reef Encounter is an underwater walking journey through our 300,000 gallon tropical reef. While wearing a state-of-the-art dive helmet that allows you to breathe freely, you'll feel right at home under the sea. During your 20-minute journey, you'll encounter tropical fish, sting rays and a variety of unique sea creatures.
								</div>
								<div class="attraction-ride-more-info">
									<a href="#">More info</a>
								</div>
							</div>
						</div>
						<div class="attraction-rides-col">
							<div class="attraction-ride">
								<div class="attraction-ride-img">
									<a href="#">
										<img src="<?= IMAGES ?>/attraction.jpg" alt="">
									</a>
								</div>
								<h3 class="attraction-ride-name">
									<a href="#">Sea Trek Reef Encounter</a>
								</h3>
								<div class="attraction-ride-description">
									Sea Trek Reef Encounter is an underwater walking journey through our 300,000 gallon tropical reef. While wearing a state-of-the-art dive helmet that allows you to breathe freely, you'll feel right at home under the sea. During your 20-minute journey, you'll encounter tropical fish, sting rays and a variety of unique sea creatures.
								</div>
								<div class="attraction-ride-more-info">
									<a href="#">More info</a>
								</div>
							</div>
						</div>
						<div class="attraction-rides-col">
							<div class="attraction-ride">
								<div class="attraction-ride-img">
									<a href="#">
										<img src="<?= IMAGES ?>/attraction.jpg" alt="">
									</a>
								</div>
								<h3 class="attraction-ride-name">
									<a href="#">Sea Trek Reef Encounter</a>
								</h3>
								<div class="attraction-ride-description">
									Sea Trek Reef Encounter is an underwater walking journey through our 300,000 gallon tropical reef. While wearing a state-of-the-art dive helmet that allows you to breathe freely, you'll feel right at home under the sea. During your 20-minute journey, you'll encounter tropical fish, sting rays and a variety of unique sea creatures.
								</div>
								<div class="attraction-ride-more-info">
									<a href="#">More info</a>
								</div>
							</div>
						</div>
						<div class="attraction-rides-col">
							<div class="attraction-ride">
								<div class="attraction-ride-img">
									<a href="#">
										<img src="<?= IMAGES ?>/attraction.jpg" alt="">
									</a>
								</div>
								<h3 class="attraction-ride-name">
									<a href="#">Sea Trek Reef Encounter</a>
								</h3>
								<div class="attraction-ride-description">
									Sea Trek Reef Encounter is an underwater walking journey through our 300,000 gallon tropical reef. While wearing a state-of-the-art dive helmet that allows you to breathe freely, you'll feel right at home under the sea. During your 20-minute journey, you'll encounter tropical fish, sting rays and a variety of unique sea creatures.
								</div>
								<div class="attraction-ride-more-info">
									<a href="#">More info</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div-->
	</section>
</section>

<?php
// End of the Main Loop.
endwhile;
get_footer(); ?>
