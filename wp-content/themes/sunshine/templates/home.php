<?php
/*
* Template Name: Home Page
*
*/
?>
<?php get_header(); ?>

<section class="hero-section">
	<div class="container">
		<div class="hero-content">
			<div class="hero-content-outer">
				<div class="hero-content-inner">
					<h1><?php the_title() ?></h1>
					<p><?php echo get_field('short_description');?></p>
					<!-- <div class="hero-cta">
            <a href="#">Explore Florida</a>
          </div> -->
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_attraction_search() ?>

<section class="tif-section attraction-section">
	<div class="container">
		<h2>Tourist Attractions in Florida</h2>
		<div class="home-carousel-outer">
			<ul id="attraction-slider" class="owl-carousel home-carousel">
				<li>
					<a href="<?php echo URL ?>/activities/amusement-parks/"class="img-container">
						<img src="<?php echo IMAGES ?>/templates/home/attraction-types/amusement-parks.jpg" alt="Amusement Parks">
						<span class="img-overlay-text">Amusement <br>Parks</span>
					</a>
				</li>
				<li>
					<a href="<?php echo URL ?>/activities/water-parks/"class="img-container">
						<img src="<?= IMAGES ?>/templates/home/attraction-types/water-parks.jpg" alt="Water Parks">
						<span class="img-overlay-text">Water <br>Parks</span>
					</a>
				</li>
				<li>
					<a href="<?php echo URL ?>/activities/beaches/"class="img-container">
						<img src="<?= IMAGES ?>/templates/home/attraction-types/beaches.jpg" alt="Beaches">
						<span class="img-overlay-text">Beaches</span>
					</a>
				</li>
				<li>
					<a href="<?php echo URL ?>/activities/zoos/"class="img-container">
						<img src="<?= IMAGES ?>/templates/home/attraction-types/zoos.jpg" alt="Zoos">
						<span class="img-overlay-text">Zoos</span>
					</a>
				</li>
				<li>
					<a href="<?php echo URL ?>/activities/theme-parks/"class="img-container">
						<img src="<?= IMAGES ?>/templates/home/attraction-types/theme-parks.jpg" alt="Theme Parks">
						<span class="img-overlay-text">Theme <br>Parks</span>
					</a>
				</li>
				<li>
					<a href="<?php echo URL ?>/activities/aquariums/"class="img-container">
						<img src="<?= IMAGES ?>/templates/home/attraction-types/aquariums.jpg" alt="Aquariums">
						<span class="img-overlay-text">Aquariums</span>
					</a>
				</li>
			</ul>
			<div class="owl-controls clearfix">
				<div class="next">
					<span class="demo-icon icon-right"></span>
				</div>
				<div class="prev">
					<span class="demo-icon icon-left"></span>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
  $attractions = Attraction::find_attractions(0,0,12,'rand');
?>
<?php if(count($attractions) > 0): ?>
<section class="tif-section">
  <div class="container">

    <div class="city-attractions-cols clearfix">
      <?php foreach($attractions as $attraction): ?>
        <?php attraction_grid_item($attraction) ?>
      <?php endforeach; ?>
    </div>

    <div class="tif-section-cta">
      <a href="<?php echo URL ?>/attractions/">View more attractions</a>
    </div>
  </div>
</section>
<?php endif; ?>

<section class="tif-section tif-blue-bg place-section">
	<div class="container">
		<h2>Find things to do in Florida by cities</h2>
		<div class="home-carousel-outer">
			<ul id="place-slider" class="owl-carousel home-carousel">
				<li>
					<a href="<?php echo URL ?>/things-to-do/orlando/"class="img-container">
						<img src="<?= IMAGES ?>/templates/home/places/orlando.jpg" alt="Orlando">
						<span class="img-overlay-text">Orlando</span>
					</a>
				</li>
				<li>
					<a href="<?php echo URL ?>/things-to-do/miami/"class="img-container">
						<img src="<?= IMAGES ?>/templates/home/places/miami.jpg" alt="Miami">
						<span class="img-overlay-text">Miami</span>
					</a>
				</li>
				<li>
					<a href="<?php echo URL ?>/things-to-do/tampa/"class="img-container">
						<img src="<?= IMAGES ?>/templates/home/places/tampa.jpg" alt="Tampa">
						<span class="img-overlay-text">Tampa</span>
					</a>
				</li>
				<li>
					<a href="<?php echo URL ?>/things-to-do/clearwater/"class="img-container">
						<img src="<?= IMAGES ?>/templates/home/places/clearwater.jpg" alt="Clearwater">
						<span class="img-overlay-text">Clearwater</span>
					</a>
				</li>
				<li>
					<a href="<?php echo URL ?>/things-to-do/jacksonville/"class="img-container">
						<img src="<?= IMAGES ?>/templates/home/places/jacksonville.jpg" alt="Jacksonville">
						<span class="img-overlay-text">Jacksonville</span>
					</a>
				</li>
				<li>
					<a href="<?php echo URL ?>/things-to-do/fort-lauderdale/"class="img-container">
						<img src="<?= IMAGES ?>/templates/home/places/fort-lauderdale.jpg" alt="Fort Lauderdale">
						<span class="img-overlay-text">Fort <br>Lauderdale</span>
					</a>
				</li>
			</ul>
			<div class="owl-controls clearfix">
				<div class="next">
					<span class="demo-icon icon-right"></span>
				</div>
				<div class="prev">
					<span class="demo-icon icon-left"></span>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $cities = City::get_all_cities(); ?>
<section class="tif-section tif-blue-bg cities-section">
  <div class="container">
    <ul class="clearfix">
      <?php foreach($cities as $city): ?>
      <li>
        <a href="<?php echo $city->get_url() ?>">
          <?php echo $city->get_name() ?>
        </a>
      </li>
      <?php endforeach; ?>
    </ul>
  </div>
</section>

<?php $blog_query = new WP_Query( array( 'posts_per_page' => 12, 'tag' => 'featured' ) ); ?>
<section class="tif-section featured-blog-section">
	<div class="container">
		<h2>Featured Blog Posts</h2>

		<div class="featured-blog-cols clearfix">
			<?php if ($blog_query->have_posts()): ?>
			<?php while($blog_query->have_posts()): $blog_query->the_post(); ?>
				<?php blog_grid_item() ?>
			<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="load-more-blogs">
			<a href="<?= URL ?>/blog/">View more articles</a>
		</div>
	</div>
</section>
<?php wp_reset_postdata(); ?>

<section class="post-content tif-section">
  <div class="container">
    <h2>Best of Florida: What to See and Do at the Sunshine State</h2>
    <div class="textcontent-section">
      <?php the_content() ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>
