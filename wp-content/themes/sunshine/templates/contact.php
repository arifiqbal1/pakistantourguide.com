<?php
/*
*	Template Name: Contact Us
*/
get_header();
while ( have_posts() ) : the_post(); ?>

<section class="contact-us-page">
	<section class="hero-section" style="background-image: url(<?php echo get_featured_image(get_the_ID()) ?>)">
		<div class="container">
			<div class="hero-content">
				<div class="hero-content-outer">
					<div class="hero-content-inner">
						<h1><?php the_title() ?></h1>
            <p>We would love to hear from you!</p>
					</div>
				</div>
			</div>
		</div>
	</section>

  <section class="contact-us-message">
    <div class="container">
      Your feedback is very important to us. <br>
      Please use the simple form below to get in touch with us.
    </div>
  </section>

  <section class="full-width contactus_form clearfix">
    <div class="contactus_form_left tif-blue-bg">
      <address>
        <div class="address_info">
          PO BOX 25423<br>
          Tamarac, FL 33320 USA
        </div>
        <div class="contact_info">
          Phone No: <strong>+1 (470) 222-4564</strong><br />
          tourisminflorida@ilmigo.com
        </div>
      </address>
    </div>

    <div class="contactus_form_right">
      <?php the_content(); ?>
      <!-- <div class="wpcf7"> -->
        <!-- <form id="contact-form"> -->
          <!-- <div class="contactus_form_row clearfix">
            <div class="tc_form_field contactus_form_col">
              <label>First Name</label>
              <input id="tc_first_name" type="text" name="tc_first_name" class="tc_standard_field" required>
            </div>
            <div class="tc_form_field contactus_form_col">
              <label>Last Name</label>
              <input id="tc_last_name" type="text" name="tc_last_name" class="tc_standard_field" required>
            </div>
          </div>

          <div class="contactus_form_row clearfix">
            <div class="tc_form_field contactus_form_col">
              <label>Phone</label>
              <input id="tc_phone_number" type="text" name="tc_phone_number" class="tc_standard_field" required>
            </div>
            <div class="tc_form_field contactus_form_col">
              <label>Email</label>
              <input id="tc_email" type="email" name="tc_email" class="tc_standard_field" required>
            </div>
          </div>

          <div class="contactus_form_row">
            <div class="tc_form_field tc_textarea_outer">
              <label>Message</label>
              <textarea id="tc_message" name="tc_message" cols="40" rows="3" required></textarea>
            </div>
          </div>

          <div class="tc_form_submit_outer">
            <button type="submit" id="tc_form_submit" class="tc-btn tc_form_submit">
              Submit
              <img src="<?= IMAGES ?>/ajax-loader.gif" alt="ajax loader">
            </button>
          </div> -->

          <!-- <div id="tc_form_field_response" class="tc_form_field_response" style="display:none">Thank you! Your message has been sent.</div>
        </form> -->
      <!-- </div> -->
    </div>
  </section>
</section>

<?php endwhile; ?>
<?php get_footer(); ?>
