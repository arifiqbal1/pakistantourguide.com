<?php
  /* Template Name: Attractions Search */
  get_header();
?>

<?php

  $cityID           = 0;
  $attractionTypeID = 0;

  if(isset($_GET["city"])) {
    $cityID = intval($_GET["city"]);
  }

  if(isset($_GET["activity"])) {
    $attractionTypeID = intval($_GET["activity"]);
  }

  $attractions = Attraction::find_attractions($cityID, $attractionTypeID);
  $totalAttractions = count($attractions);

  $pluralResults = "s";
  if($totalAttractions == 1) $pluralResults = "";

  $heading = "We have found {$totalAttractions} attraction{$pluralResults} for you..!";

  if($totalAttractions <= 0) {
    $heading = "Looks like we couldn't find any attractions for your query..!";
  }

  $showReviews = false;


?>
<section class="hero-section">
	<div class="container">
		<div class="hero-content">
			<div class="hero-content-outer">
				<div class="hero-content-inner">
					<h1><?php echo $heading ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_attraction_search($cityID, $attractionTypeID) ?>

<section class="attractions-search-results tif-blue-bg tif-section">
  <div class="container">
    <div class="city-attractions-cols clearfix">
      <?php foreach($attractions as $attraction): ?>
        <div class="city-attractions-col">
          <div class="city-attraction">
            <div class="city-attraction-img">
              <a href="<?= $attraction->get_url() ?>">
                <img src="<?= $attraction->get_image('medium') ?>" alt="<?= $attraction->get_name() ?>">
              </a>
            </div>
            <div class="city-attraction-meta">
              <div class="city-attraction-name">
                <a href="<?= $attraction->get_url() ?>"><?= $attraction->get_name() ?></a>
              </div>
            </div>
            <div class="city-attraction-address">
              <span class="address-icon icon-location"></span>
              <span class="attraction-address-text">
                <?= $attraction->get_fulladdress() ?>
              </span>
            </div>

            <?php if($showReviews): ?>
              <!-- <div class="city-attraction-reviews">
                <span class="review-average">4.6</span>
                <span class="review-stars">
                  <span class="rating">
                    <span class="star rating-6"></span>
                    <span class="star rating-7"></span>
                    <span class="star rating-8"></span>
                    <span class="star rating-9"></span>
                    <span class="star rating-10"></span>
                  </span>
                </span>
                <span class="reviews-count">
                  <a href="#">536 reviews</a>
                </span>
              </div> -->
            <?php endif; ?>

          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>
