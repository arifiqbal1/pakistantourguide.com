<?php
/* Template Name: Without Hero */

get_header('without-hero');
while ( have_posts() ) : the_post();
?>

<section class="breadcrumb-section">
  <div class="container">
    <ol itemscope itemtype="http://schema.org/BreadcrumbList" class="clearfix">
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a href="<?= URL ?>" itemprop="item">
          <span itemprop="name">Home</span>
        </a>
      </li>
      <li>
        <span><?php the_title() ?></span>
      </li>
    </ol>
  </div>
</section>

<section class="post-content tif-section">
  <div class="container">
    <h1><?php the_title() ?></h1>
    <?php the_content() ?>
  </div>
</section>
<?php endwhile; ?>
<?php get_footer(); ?>
