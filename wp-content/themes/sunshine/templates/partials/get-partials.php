<?php

function get_attraction_search($cityID=0, $attractionTypeID=0) {

  $cities 		= City::get_all_cities();
  $activities = Activity::get_all_activities();
  ?>

  <section class="attraction-search">
  	<div class="container">
  		<form class="" action="<?php echo URL ?>/attractions/" method="get">
  			<div class="clearfix search-inner">
  				<div class="search-text">
  					<img src="<?= IMAGES ?>/icons/search-icon.png">
  					<h3>Find Attractions</h3>
  				</div>
  				<div class="search-boxes box-1">
  					<select class="tif-select2" placeholder="Cities" name="city">
  						<option value="0">Any City</option>

  						<?php foreach($cities as $city): ?>
  							<option value="<?php echo $city->ID ?>" <?php echo ($cityID == $city->ID)? "selected" : "" ?>>
                  <?php echo $city->get_name() ?>
                </option>
  						<?php endforeach; ?>

  					</select>
  				</div>
  				<div class="search-boxes box-2">
  					<select class="tif-select2" placeholder="Any Activity" name="activity">
  						<option value="0">Any Activity</option>

  						<?php foreach($activities as $activity): ?>
  							<option value="<?php echo $activity->ID ?>" <?php echo ($attractionTypeID == $activity->ID)? "selected" : "" ?>>
                  <?php echo $activity->get_name() ?>
                </option>
  						<?php endforeach; ?>

  					</select>
  				</div>
  				<div class="search-submit">
  					<button type="submit">Search</button>
  				</div>
  			</div>
  		</form>
  	</div>
  </section>

  <?php
}


function get_main_nav($logo="logo.png") {
  ?>

  <div class="header-cols clearfix">
    <div class="header-left">
      <a href="<?= URL ?>" title="Go to Homepage">
        <img src="<?php echo IMAGES . "/" . $logo ?>" alt="<?= get_bloginfo() ?>">
      </a>
    </div>
    <div class="header-middle">
      <!-- <nav id="tif-nav" class="tif-nav">
        <ul class="clearfix">
          <li class="has-children">
            <a href="#">Things to do</a>
            <ul>
              <li><a href="<?= URL ?>/things-to-do/cape-coral/">Cape Coral</a></li>
              <li><a href="<?= URL ?>/things-to-do/clearwater/">Clearwater</a></li>
              <li><a href="<?= URL ?>/things-to-do/coral-springs/">Coral Springs</a></li>
              <li><a href="<?= URL ?>/things-to-do/fort-lauderdale/">Fort Lauderdale</a></li>
              <li><a href="<?= URL ?>/things-to-do/gainesville/">Gainesville</a></li>
              <li><a href="<?= URL ?>/things-to-do/hialeah/">Hialeah</a></li>
              <li><a href="<?= URL ?>/things-to-do/hollywood/">Hollywood</a></li>
              <li><a href="<?= URL ?>/things-to-do/jacksonville/">Jacksonville</a></li>
              <li><a href="<?= URL ?>/things-to-do/miami/">Miami</a></li>
              <li><a href="<?= URL ?>/things-to-do/miami-gardens/">Miami Gardens</a></li>
              <li><a href="<?= URL ?>/things-to-do/miramar/">Miramar</a></li>
              <li><a href="<?= URL ?>/things-to-do/orlando/">Orlando</a></li>
            </ul>
          </li>
           <li class="has-children">
            <a href="#">Attractions</a>
            <ul>
              <li><a href="#">Theme Parks</a></li>
              <li><a href="#">Water Parks</a></li>
              <li><a href="#">Zoos</a></li>
              <li><a href="#">Theme Parks</a></li>
              <li><a href="#">Water Parks</a></li>
              <li><a href="#">Zoos</a></li>
              <li><a href="#">Theme Parks</a></li>
              <li><a href="#">Water Parks</a></li>
              <li><a href="#">Zoos</a></li>
              <li><a href="#">Theme Parks</a></li>
              <li><a href="#">Water Parks</a></li>
              <li><a href="#">Zoos</a></li>
            </ul>
          </li>
          <li><a href="<?= URL ?>/contact/">Contact Us</a></li>
        </ul>
      </nav> -->
    </div>
    <div class="header-right-mobile">
      <button id="hamburger-menu" class="hamburger-menu"><span class="icon-menu"></span></button>
    </div>
    <div class="header-right clearfix">
      <nav id="tif-nav" class="tif-nav">
        <ul class="clearfix">
          <li class="has-children">
            <a href="#">Cities</a>
            <ul>
              <li><a href="<?= URL ?>/things-to-do/cape-coral/">Cape Coral</a></li>
              <li><a href="<?= URL ?>/things-to-do/clearwater/">Clearwater</a></li>
              <li><a href="<?= URL ?>/things-to-do/cocoa-beach/">Cocoa Beach</a></li>
              <li><a href="<?= URL ?>/things-to-do/coral-springs/">Coral Springs</a></li>
              <li><a href="<?= URL ?>/things-to-do/delray-beach/">Delray Beach</a></li>
              <li><a href="<?= URL ?>/things-to-do/florida-keys/">Florida Keys</a></li>
              <li><a href="<?= URL ?>/things-to-do/fort-lauderdale/">Fort Lauderdale</a></li>
              <li><a href="<?= URL ?>/things-to-do/gainesville/">Gainesville</a></li>
              <li><a href="<?= URL ?>/things-to-do/hialeah/">Hialeah</a></li>
              <li><a href="<?= URL ?>/things-to-do/hollywood/">Hollywood</a></li>
              <li><a href="<?= URL ?>/things-to-do/jacksonville/">Jacksonville</a></li>
              <li><a href="<?= URL ?>/things-to-do/melbourne/">Melbourne</a></li>
              <li><a href="<?= URL ?>/things-to-do/miami/">Miami</a></li>
              <li><a href="<?= URL ?>/things-to-do/miami-gardens/">Miami Gardens</a></li>
              <li><a href="<?= URL ?>/things-to-do/miramar/">Miramar</a></li>
              <li><a href="<?= URL ?>/things-to-do/orlando/">Orlando</a></li>
            </ul>
          </li>
           <li class="has-children">
            <a href="#">Activities</a>
            <ul>
              <li><a href="<?php echo URL ?>/activities/amusement-parks/">Amusement Parks</a></li>
              <li><a href="<?php echo URL ?>/activities/aquariums/">Aquariums</a></li>
              <li><a href="<?php echo URL ?>/activities/caves/">Caves</a></li>
              <li><a href="<?php echo URL ?>/activities/theme-parks/">Theme Parks</a></li>
              <li><a href="<?php echo URL ?>/activities/water-parks/">Water Parks</a></li>
  						<li><a href="<?php echo URL ?>/activities/zoos/">Zoos</a></li>
            </ul>
          </li>
          <li><a href="<?= URL ?>/blog/">Blog</a></li>
          <!-- <li><a href="<?= URL ?>/contact/">Contact Us</a></li> -->
        </ul>
      </nav>

      <!-- <div class="tif-membership clearfix">
        <a href="#" class="tif-register">
          <span class="icon-user-outline"></span> Register
        </a>
        <a href="#" class="tif-login">
          <span class="icon-login"></span> Login
        </a>
      </div> -->
    </div>
  </div>

  <?php
}

// for menu of categories
function get_cats_menu() {
?>
<section class="blog-menu">
  <div class="container">
    <div class="blog-cats-mobile-menu">
      <a href="#" class="clearfix tif-collapse" data-target="#featured-categories">Blog Categories <span class="icon-down-open"></span></a>
    </div>
    <ul id="featured-categories" class="clearfix featured-categories">
      <li><a href="<?= URL ?>/things-to-do/florida-keys/">Florida Keys<span class="cat-lightblue"></span></a></li>
      <li><a href="<?= URL ?>/things-to-do/cocoa-beach/">Cocoa Beach<span class="cat-red"></span></a></li>
      <li><a href="<?= URL ?>/things-to-do/melbourne/">Melbourne<span class="cat-blue"></span></a></li>
      <li><a href="<?= URL ?>/things-to-do/delray-beach/">Delray Beach<span class="cat-green"></span></a></li>
      <li><a href="<?= URL ?>/activities/caves/">Caves in Florida<span class="cat-orange"></span></a></li>
    </ul>
 </div>
</section>
<?php
}


//get single attraction item
function attraction_grid_item($attraction) {
  $showReviews = false; ?>

  <div class="city-attractions-col">
    <div class="city-attraction">
      <div class="city-attraction-img">
        <a href="<?= $attraction->get_url() ?>">
          <?php if (has_post_thumbnail($attraction->ID)) : ?>
          <img src="<?= $attraction->get_image('medium') ?>" alt="<?= $attraction->get_name() ?>">
          <?php else: ?>
          <img src="<?= IMAGES ?>/no-image.png" alt="<?= $attraction->get_name() ?>">
          <?php endif; ?>
        </a>
      </div>
      <div class="city-attraction-meta">
        <!--<div class="city-attraction-type">
        <?php
          $a_category = $attraction->get_category();
          if($a_category) {
            //echo "<a href='{$a_category->get_url()}'>{$a_category->get_name()}</a>";
          }
        ?>
        </div> -->
        <div class="city-attraction-name">
          <a href="<?= $attraction->get_url() ?>" title="<?= $attraction->get_name() ?>"><?= $attraction->get_name() ?></a>
        </div>
      </div>
      <div class="city-attraction-address">
        <span class="address-icon icon-location"></span>
        <span class="attraction-address-text">
          <?= $attraction->get_fulladdress() ?>
        </span>
      </div>

      <?php if($showReviews): ?>
        <!-- <div class="city-attraction-reviews">
          <span class="review-average">4.6</span>
          <span class="review-stars">
            <span class="rating">
              <span class="star rating-6"></span>
              <span class="star rating-7"></span>
              <span class="star rating-8"></span>
              <span class="star rating-9"></span>
              <span class="star rating-10"></span>
            </span>
          </span>
          <span class="reviews-count">
            <a href="#">536 reviews</a>
          </span>
        </div> -->
      <?php endif; ?>
    </div>
  </div>
  <?php
}



//get single blog grid item
function blog_grid_item() {
  ?>

  <div class="featured-blog-col">
    <div class="featured-blog">
      <div class="featured-blog-img img-container">
        <?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('blogpost-thumb'); ?>
              <?php endif; ?>
      </div>
      <div class="featured-blog-date">
        <p>Published on <?= get_the_date('F j') ?></p>
      </div>
      <h3 class="featured-blog-name">
        <a href="<?= get_permalink() ?>"><?= get_the_title() ?></a>
      </h3>
      <div class="featured-blog-description">
        <p><?php the_field('short_description') ?></p>
      </div>
      <div class="featured-blog-more-info">
        <a href="<?= get_permalink() ?>">View Post</a>
      </div>
    </div>
  </div>
  <?php
}
