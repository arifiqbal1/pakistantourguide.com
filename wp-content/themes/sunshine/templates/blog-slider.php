<?php

$catSlug = "";
if(is_category()) {
  $cat = get_query_var('cat');
  $cat = get_category ($cat);
  $catSlug = $cat->slug;
}
$args = array (
  'category_name'   => $catSlug,
  'tag'             => 'featured',
  'posts_per_page'  => 10
);
$sliderposts = new WP_Query($args);
$foundPost   = $sliderposts->found_posts;
?>

<?php if($sliderposts->have_posts()): ?>
<div class="blog-carousel-outer">
  <ul id="blog-slider" class="owl-carousel blog-carousel" data-slidescount="<?php echo $foundPost ?>">
    <?php while($sliderposts->have_posts()): $sliderposts->the_post(); ?>
      <li class="clearfix">
        <div class="featured-blog-img-container">
          <?php if(has_post_thumbnail()){ ?>
            <a href="<?php echo get_the_permalink(); ?>">
              <?php the_post_thumbnail(); ?>
            </a>
          <?php } ?>
        </div>
        <div class="featured-blog-data-container">
          <div class="featured-blog-title">
            <h2>
              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h2>
          </div>
          <div class="featured-blog-short-content">
            <p><?php the_field('short_description') ?></p>
          </div>
        </div>
      </li>
    <?php endwhile; ?>
  </ul>
  <?php if($foundPost>1): ?>
  <div class="owl-controls clearfix">
    <div class="prev">
      <span class="icon-left-open"></span>
    </div>
    <div class="next">
      <span class="icon-right-open"></span>
    </div>
    <span class="slide-numbers"><span id="currentSlidePost">1</span> of <?php echo $foundPost ?></span>
  </div>
  <?php endif; ?>
</div>
<?php wp_reset_postdata(); ?>
<?php endif; ?>
