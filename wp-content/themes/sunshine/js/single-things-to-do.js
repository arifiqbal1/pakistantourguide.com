$(function() {
  $("#avg-tempratures").change(function() {
    var tempratures = $(this).val();
    tempratures     = tempratures.split('|');
    $("#low-temperature").text(tempratures[0]);
    $("#high-temperature").text(tempratures[1]);
  });
});
