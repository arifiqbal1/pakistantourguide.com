$(function() {

  // $("#select2-city").select2({
  //   placeholder: "Cities",
  //   allowClear: true
  // });
  // $("#select2-attraction").select2({
  //   placeholder: "Attraction",
  //   allowClear: true
  // });

  //Sliders
  var args = {
    items : 4, //4 items above 1000px browser width
    itemsDesktop : [1279,3], //4 items between 1000px and 901px
    itemsDesktopSmall : [1024,3], // 3 items betweem 900px and 601px
    itemsTablet: [1023,2], //2 items between 600 and 0;
    itemsMobile : [767,1], // itemsMobile disabled - inherit from itemsTablet option
    dots: false,
    pagination: false
  };

  var attractionCarousel = $("#attraction-slider");
  attractionCarousel.owlCarousel(args);
  //Custom Navigation Events
  $(".attraction-section .next").click(function(){
    attractionCarousel.trigger('owl.next');
  });
  $(".attraction-section .prev").click(function(){
    attractionCarousel.trigger('owl.prev');
  });

  var placeCarousel = $("#place-slider");
  placeCarousel.owlCarousel(args);
  $(".place-section .next").click(function(){
    placeCarousel.trigger('owl.next');
  });
  $(".place-section .prev").click(function(){
    placeCarousel.trigger('owl.prev');
  });

});
