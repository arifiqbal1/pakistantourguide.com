$(function() {

	//Reviews Slider
	var owl = $("#owl-review");
    owl.owlCarousel({
      items : 3, //10 items above 1000px browser width
      itemsDesktop : [1000,3], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0;
      itemsMobile : [500,1] // itemsMobile disabled - inherit from itemsTablet option
    });

    //Custom Navigation Events
    $(".next").click(function(){
        owl.trigger('owl.next');
     })
    $(".prev").click(function(){
        owl.trigger('owl.prev');
    })
    $(".play").click(function(){
        owl.trigger('owl.play',1000);
    })
    $(".stop").click(function(){
        owl.trigger('owl.stop');
    })

	//FAQs
	$('.faq i.icon-up-open').slideUp();
  	$('.faq li a').click(function(event) {
  		event.preventDefault();
  		$(this).parent().children('.faq-para').slideToggle(200, function(){
  			if($(this).is(':visible')){
  				$(this).parent().addClass('faq-open');
  				$(this).parent().children('a').children('i.icon-down-open').hide();
  				$(this).parent().children('a').children('i.icon-up-open').show();
  			}
  			else {
  				$(this).parent().removeClass('faq-open');
  				$(this).parent().children('a').children('i.icon-down-open').show();
  				$(this).parent().children('a').children('i.icon-up-open').hide();
  			}
  	});
	});

	$.simpleWeather({
		location: $('.attraction-address-text').text(),
		woeid: '',
		unit: 'f',
		success: function(weather) {
			$('.weather-report-wait').hide();
			$('.weather-report-data').show();
			//html = '<h2><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'</h2>';
			//html += '<ul><li>'+weather.city+', '+weather.region+'</li>';
			///html += '<li class="currently">'+weather.currently+'</li>';
			//html += '<li>'+weather.wind.direction+' '+weather.wind.speed+' '+weather.units.speed+'</li></ul>';
			$('.current-temprature').html(weather.temp);
			$('.temprature-units').html('&deg;'+weather.units.temp);
			$('.current-weather-icon').html('<img src="'+weather.image+'">');
			$('.current-temprature.temp-celsius').html(weather.alt.temp);
			$('.temprature-units.temp-celsius').html('&deg;'+weather.alt.unit);

		},
		error: function(error) {
			$('.weather-report-wait').html('<p>'+error+'</p>');
		}
	});

});
