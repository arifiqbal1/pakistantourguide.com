$(function() {

	var args = {
	 	animateOut: 'fadeOut',
		animateIn: 'flipInX',
		items:1,
		// loop: true,
		// autoplay: true,
		// autoplayTimeout: 3000
	};
	var blogCarousel = $("#blog-slider");
	blogCarousel.owlCarousel(args);
	$(".blog-section .next").click(function(){
    blogCarousel.trigger('next.owl.carousel');
	});
	$(".blog-section .prev").click(function(){
	  blogCarousel.trigger('prev.owl.carousel');
	});
	$(".blog-carousel-outer .owl-controls").show();

	blogCarousel.on('changed.owl.carousel', function(event) {
	  var currentItem = event.item.index;
		$('#currentSlidePost').text(currentItem + 1);
	})

});
/*var $grid;
var container = document.querySelector('.featured-blog-cols');
var msnry = new Masonry( container, {
		itemSelector: '.ms-item',
		columnWidth: '.ms-item',
	});
jQuery(document).ready(function($) {

	 $grid = $('.grid').isotope({
	  masonry: {
	    columnWidth: 50
	  }
	});

	$("#filters-buttons button").on("click",function(event){
		event.preventDefault();
		grid_layout_masonry();
	});

		 // MASSONRY Without jquery



	// filter functions
		var filterFns = {
		  // show if number is greater than 50
		  numberGreaterThan50: function() {
		    var number = $(this).find('.number').text();
		    return parseInt( number, 10 ) > 50;
		  },
		  // show if name ends with -ium
		  ium: function() {
		    var name = $(this).find('.name').text();
		    return name.match( /ium$/ );
		  }
		};
	// bind filter on select change
	$('.filters-select').on( 'change', function() {
		  // get filter value from option value
		  var filterValue = this.value;
		  // use filterFn if matches value
		  filterValue = filterFns[ filterValue ] || filterValue;
		  $grid.isotope({ filter: filterValue });
		  grid_layout_masonry();
	});
	grid_layout_masonry();
});
function grid_layout_masonry(){
		//$grid.isotope('reloadItems');
		//$grid.isotope('layout');
		//$grid.isotope('destroy');
		msnry = new Masonry( container, {
		 itemSelector: '.ms-item',
		 columnWidth: '.ms-item',
		});
}*/
