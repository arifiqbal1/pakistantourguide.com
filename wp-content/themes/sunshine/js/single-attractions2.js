jQuery(function() {

  var lat = jQuery('#tourist-attraction').data('lat');
  var lng = jQuery('#tourist-attraction').data('lng');

  if(jQuery('#attraction-map').length > 0) {
  	jQuery('#attraction-map')
      .gmap3({
        center:[lat, lng],
        zoom:15
      })
      .marker([
        {position:[lat, lng]},
        //{address:"86000 Poitiers, France"},
      ])
      .on('click', function (marker) {
        //marker.setIcon('http://maps.google.com/mapfiles/marker_green.png');
    });
  }

  //Smooth scroll to sections
  jQuery('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
        && 
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = jQuery(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          jQuery('html, body').animate({
            scrollTop: target.offset().top - 30
          }, 1000, function() {
            // Callback after animation
            // Must change focus!
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) { // Checking if the target was focused
              return false;
            } else {
              $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
              $target.focus(); // Set focus again
            };
          });
        }
      }
    });

    jQuery(window).scroll(function() {
      if(jQuery(window).scrollTop() > 1024) {
        jQuery('#attraction-nav').addClass('fixed-attraction-nav');
      }
      else {
        jQuery('#attraction-nav').removeClass('fixed-attraction-nav');
      }
    });
    
    /*jQuery("#owl-demo").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });
  $( ".owl-prev").html('<span class="demo-icon icon-left"></span>');
  $( ".owl-next").html('<span class="demo-icon icon-right"></span>');*/
    
  /*var owl_demo = $("#owl-demo");
  var owl_demo2 = $("#owl-demo-thumbs");
  var slidesPerPage = 4; //globaly define number of elements per page
  var syncedSecondary = true;

  owl_demo.owlCarousel({
    items : 1,
    slideSpeed : 2000,
    nav: true,
    autoplay: true,
    dots: true,
    loop: true,
    responsiveRefreshRate : 200,
    navText: ['<span class="demo-icon icon-left"></span>','<span class="demo-icon icon-right"></span>'],
  }).on('changed.owl.carousel', syncPosition);

  owl_demo2
    .on('initialized.owl.carousel', function () {
      owl_demo2.find(".owl-item").eq(0).addClass("current");
    })
    .owlCarousel({
    items : slidesPerPage,
    dots: true,
    nav: true,
    smartSpeed: 200,
    slideSpeed : 500,
    slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
    responsiveRefreshRate : 100,
    navText: ['<span class="demo-icon icon-left"></span>','<span class="demo-icon icon-right"></span>'],
  }).on('changed.owl.carousel', syncPosition2);

  function syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;
    
    //if you disable loop you have to comment this block
    var count = el.item.count-1;
    var current = Math.round(el.item.index - (el.item.count/2) - .5);
    
    if(current < 0) {
      current = count;
    }
    if(current > count) {
      current = 0;
    }
    
    //end block

    owl_demo2
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = owl_demo2.find('.owl-item.active').length - 1;
    var start = owl_demo2.find('.owl-item.active').first().index();
    var end = owl_demo2.find('.owl-item.active').last().index();
    
    if (current > end) {
      owl_demo2.data('owl.carousel').to(current, 100, true);
    }
    if (current < start) {
      owl_demo2.data('owl.carousel').to(current - onscreen, 100, true);
    }
  }
  
  function syncPosition2(el) {
    if(syncedSecondary) {
      var number = el.item.index;
      owl_demo.data('owl.carousel').to(number, 100, true);
    }
  }
  
  owl_demo2.on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).index();
    owl_demo.data('owl.carousel').to(number, 300, true);
  });*/

});
$(window).load(function() {
  // The slider being synced must be initialized first
  $('#slider-thumbs').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 210,
    itemMargin: 5,
    asNavFor: '#slider',
    prevText: '<span class="demo-icon icon-left"></span>',
    nextText: '<span class="demo-icon icon-right"></span>'
  });
 
  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#slider-thumbs",
    prevText: '<span class="demo-icon icon-left"></span>',
    nextText: '<span class="demo-icon icon-right"></span>'
  });
});