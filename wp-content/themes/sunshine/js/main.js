var $ = jQuery;

function mobileMenu() {
	var screenW = window.innerWidth;

	if(screenW < 1024) {
		$('#tif-nav > ul').appendTo('#mobile-menu');
	}
}

function tifSelct(tifSelect) {
	var selectedText = $(tifSelect).find(":selected").text();
	$(tifSelect).next().text(selectedText);
}

//Tabs
var screenW = window.innerWidth;
if(screenW < 768) {
$('.tab-content .tab-pane').removeClass('active');
}

$('.tab-expand-mobile > a').click(function() {

var element = this;
setTimeout(function(){
    var target = $(element.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    if (target.length) {
      $('html,body').animate({
        scrollTop: target.offset().top - 47
      }, 500);
      //return false;
    }
}, 100);

});


function validateEmail(Email) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

    if ($.trim(Email).length == 0) {
        return false;
    }
    if(filter.test(Email)){
        return true;
    }else {
        return false;
    }
}


$(function() {
	mobileMenu();

	$('#hamburger-menu').click(function() {
		$('body').addClass('mobile-menu-open');
	});
	$('#mobile-menu li.has-children > a').click(function(e) {
		e.preventDefault();
		var parent  = $(this).parent('.has-children');
		var ul		= $(parent).find('ul');

		$(ul).slideToggle();
	});
	$('#mobile-menu-close').click(function() {
		$('body').removeClass('mobile-menu-open');
	});
	$('.footer-list > h3').click(function() {
		var parent  = $(this).parent('.footer-list');
		var ul		= $(parent).find('ul');
		$(parent).toggleClass('open');
		$(ul).slideToggle();
	});
	$('.tif-dd > select').change(function() {
		tifSelct($(this));
	});
	$('.tif-dd > select').each(function(index, element) {
		tifSelct($(element));
	});
	//Footer newsletter
	$('#footer_newsletter_btn').click(function () {
		var email = $('#footer_newsletter_email').val();
		if(validateEmail(email)){
			$('#footer-newsletter-form p.error').hide();
			$('#footer-newsletter-form').slideUp();
			$('#footer-newsletter-form-sending').show();

			var data = {
				"email"			: email
			};

			$.ajax({
				cache	: false,
				url		: TIF_Ajax.ajaxurl,
				type	: "POST",
				data	: {
					action		: 'tifNewsletterSubscribe',
					data		: data,
					security	: TIF_Ajax.tif_nonce,
				},

				success: function( data, textStatus, jqXHR ){
					//console.log(data);
					if(data == "1") {
						$('#footer-newsletter-form-sending').slideUp();
						$('#footer-newsletter-form-sent').slideDown();
					}
					else {
					}
				}
			});
			return false;
		}else{
			$('#footer-newsletter-form p.error').text('Enter a valid email address');
		}
	});

	$('.tif-collapse').click(function() {
		var target = $(this).data('target');
		$(target).slideToggle();
	});
});
