<?php
require_once 'inc/globals.php';
require_once 'inc/model.php';
require_once 'inc/enqueues.php';
require_once 'inc/newsletters.php';
require_once 'inc/shortcodes.php';
require_once 'inc/routing.php';

require_once 'inc/sliderposts.php';

require_once 'templates/partials/get-partials.php';

function tif_attraction_search_vars( $qvars ) {
  $qvars[] = 'city';
  $qvars[] = 'activity';
  return $qvars;
}
add_filter( 'query_vars', 'tif_attraction_search_vars' , 10, 1 );
