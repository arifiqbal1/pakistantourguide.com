<section class="footer-widgets">
	<div class="footer-widgets-border">
		<div class="footer-border-inner"></div>
	</div>
	<div class="container">
		<div class="footer-cols clearfix">
			<div class="footer-list-cols clearfix">
				<div class="footer-list">
					<h3>Things to do</h3>
					<ul>
						<li><a href="<?= URL ?>/things-to-do/cocoa-beach/">Cocoa Beach</a></li>
						<li><a href="<?= URL ?>/things-to-do/delray-beach/">Delray Beach</a></li>
						<li><a href="<?= URL ?>/things-to-do/florida-keys/">Florida Keys</a></li>
						<li><a href="<?= URL ?>/things-to-do/melbourne/">Melbourne</a></li>
						<li><a href="<?= URL ?>/things-to-do/orlando/">Orlando</a></li>
					</ul>
				</div>
				<div class="footer-list">
					<h3>Activities</h3>
					<ul>
						<li><a href="<?php echo URL ?>/activities/amusement-parks/">Amusement Parks</a></li>
						<li><a href="<?php echo URL ?>/activities/aquariums/">Aquariums</a></li>
						<li><a href="<?php echo URL ?>/activities/caves/">Caves</a></li>
						<li><a href="<?php echo URL ?>/activities/theme-parks/">Theme Parks</a></li>
						<li><a href="<?php echo URL ?>/activities/water-parks/">Water Parks</a></li>
						<li><a href="<?php echo URL ?>/activities/zoos/">Zoos</a></li>
					</ul>
				</div>
				<div class="footer-list">
					<h3>About</h3>
					<ul>
						<!-- <li><a href="#">Blog</a></li>
						<li><a href="#">About Us</a></li> -->
						<li><a href="<?php echo URL ?>/about-florida/">About Florida</a></li>
						<li><a href="<?php echo URL ?>/contact-us/">Contact Us</a></li>
						<li><a href="<?php echo URL ?>/privacy-policy/">Privacy Policy</a></li>
						<li><a href="<?php echo URL ?>/terms/">Terms of Us</a></li>
					</ul>
				</div>
			</div>

			<div class="footer-devider"></div>

			<div class="footer-newsletter">
				<div id="footer-newsletter-form" class="footer-newsletter-form">
					<h3>Newsletter subscribe</h3>
					<p>Subscribe to stay updated with latest news and events about tourism in Florida.</p>
					<div class="footer-newsletter-form">
						<input  id="footer_newsletter_email" type="text" placeholder="E-mail" />
						<button id="footer_newsletter_btn"><span class="icon-right-open"></span></button>
					</div>
					<p class="error"></p>
				</div>
				<div id="footer-newsletter-form-sending" style="display:none">
					<div class="form-sending-icon">
						<span class="icon-spin animate-spin"></span>
						<p>Signing you up!</p>
					</div>
				</div>
				<div id="footer-newsletter-form-sent" class="footer-newsletter-form-sent" style="display:none">
					<div class="form-sent-icon">
						<span class="icon-ok"></span>
					</div>
					<h3>Thank you!</h3>
					<p>You've been subscribed. <br>Please check your email for confirmation.</p>
				</div>
			</div>
			<div class="footer-social">
				<ul class="clearfix">
					<li><a href="https://www.facebook.com/TourismFL/" target="_blank" rel="me nofollow"><span class="icon-facebook"></span></a></li>
					<li><a href="https://twitter.com/TourismFL" target="_blank" rel="me nofollow"><span class="icon-twitter"></span></a></li>
					<li><a href="https://plus.google.com/+Tourisminflorida" target="_blank" rel="me nofollow"><span class="icon-gplus"></span></a></li>
					<li><a href="http://www.youtube.com/c/Tourisminflorida" target="_blank" rel="me nofollow"><span class="icon-youtube-play"></span></a></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<footer class="tif-footer">
	<div class="container">&copy; <?php echo date("Y"); ?> TourismInFlorida.com</div>
</footer>

<nav id="mobile-menu" class="mobile-menu">
	<div class="mobile-menu-close-outer">
		<button id="mobile-menu-close" class="mobile-menu-close"><span class="icon-cancel"></span></button>
	</div>
</nav>

<?php wp_footer(); ?>
</body>
</html>
