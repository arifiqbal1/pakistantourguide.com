<?php
  $amp = false; 
  if(isset($_GET["amp"])) $amp = true;
?>

<?php
  get_header('without-hero');
?>

<?php get_cats_menu(); ?>

<?php if ( function_exists('yoast_breadcrumb') ) : ?>
<section class="breadcrumb-section">
  <div class="container">
    
    <ol itemscope itemtype="http://schema.org/BreadcrumbList" class="clearfix">
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a href="<?= URL ?>" itemprop="item">
          <span itemprop="name">Home</span>
        </a>
      </li>

      <?php if(get_field('article_type') == '2'): ?>
        <?php
          $activity = get_field('article_primary_activity');
        ?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a href="<?= URL ?>/activities/<?= $activity->slug ?>/" itemprop="item">
            <span itemprop="name"><?= $activity->name ?> in Florida</span>
          </a>
        </li>
      <?php elseif(get_field('article_type') == '1'): ?>
        <?php
          $city = get_field('article_primary_city');
        ?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a href="<?= URL ?>/things-to-do/<?= $city->post_name ?>/" itemprop="item">
            <span itemprop="name">Things to do in <?= $city->post_title ?></span>
          </a>
        </li>

      <?php elseif(get_field('article_type') == '3'): ?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a href="<?= URL ?>/travel-tips/" itemprop="item">
            <span itemprop="name">Florida Travel Tips</span>
          </a>
        </li>
      <?php endif; ?>

      <li>
        <span><?= get_the_title();?></span>
      </li>
    </ol>
    
  </div>
</section>
<?php endif; ?>

<section class="blog-section">
  <div class="container clearfix">
    <div class="blog-content-area">
      <?php while ( have_posts() ) : the_post(); ?>
      <article class="single-post-section">
        <h1 itemprop="name" class="post-title"><?php the_title() ?></h1>
        <p class="post-overview"><?php the_field('short_description') ?></p>
        <?php if (has_post_thumbnail()) : ?>
          <?php the_post_thumbnail(); ?>
        <?php endif; ?>
        <div class="post-content content">
          <?php the_content() ?>
        </div>

        <?php
        $activities = get_the_terms(get_the_ID(), 'activities');
        if ( $activities ):
        ?>
        <div class="post-activity-tags">
          <strong>Post tags: </strong>
          <?php

          foreach($activities as $activity) {
            echo '<span><a href="'.URL.'/activities/'.$activity->slug.'/">'.$activity->name.'</a></span>';
          }
          ?>
        </div>
        <?php endif; ?>

      </article>
      <?php endwhile; ?>
    </div>
    <?php get_sidebar(); ?>
  </div>
</section>

<?php

  $postIDs = array();

  $related_ids    = get_field('article_relations', false, false);
  $related_posts  = get_posts( array(
                      'post_type' => 'post',
                      'posts_per_page' => -1,
                      'post__in' => $related_ids
                    ));

  $related_posts2 = get_posts( array(
                      'post_type' => 'post',
                      'posts_per_page' => -1, 
                      'meta_query' => array(
                          'relation' => 'OR',
                          array(
                              'key' => 'article_relations', // name of custom field
                              'value' => '"' . get_the_ID() . '"',
                              'compare' => 'LIKE'
                          )
                        )
                    ));
  ?>
  <?php if ($related_posts || $related_posts2): ?>
  <section class="featured-blog-section tif-section">
    <div class="container">
      <h3 class="related-heading">Related Reading</h3>

      <div class="featured-blog-cols clearfix">
      <?php 
        foreach($related_posts as $post) {
            setup_postdata( $post );

            array_push($postIDs, get_the_ID());

            blog_grid_item();
            wp_reset_postdata();
        }

        foreach($related_posts2 as $post){
          setup_postdata( $post );
          if (!in_array(get_the_ID(), $postIDs)) {
            blog_grid_item();
          }
          wp_reset_postdata();
        }
      ?>
      </div>
    </div>
  </section>
  <?php endif; ?>
  <?php wp_reset_postdata(); ?>

<?php get_footer(); ?>
