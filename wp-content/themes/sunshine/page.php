<?php

get_header();
while ( have_posts() ) : the_post();
?>

<section class="hero-section" style="background-image: url(<?php echo get_featured_image(get_the_ID()) ?>)">
  <div class="container">
    <div class="hero-content">
      <div class="hero-content-outer">
        <div class="hero-content-inner">
          <h1 itemprop="name"><?php the_title() ?></h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="breadcrumb-section">
  <div class="container">
    <ol itemscope itemtype="http://schema.org/BreadcrumbList" class="clearfix">
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a href="<?= URL ?>" itemprop="item">
          <span itemprop="name">Home</span>
        </a>
      </li>
      <li>
        <span><?php the_title() ?></span>
      </li>
    </ol>
  </div>
</section>

<section class="post-content tif-section">
  <div class="container">
    <?php the_content() ?>
  </div>
</section>

<?php endwhile; ?>
<?php get_footer(); ?>
