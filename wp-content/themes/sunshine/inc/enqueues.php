<?php

add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );


/*
Enqueue CSS/JS files
---------------------------------------*/
function modify_jquery() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', false, '2.2.4');
		wp_enqueue_script('jquery');
	}
}
add_action('init', 'modify_jquery');

/*
Enqueue CSS/JS files
---------------------------------------*/
function tourismfl_enqueues()
{
	//Theme font files.
	//wp_enqueue_style( 'fonts-googleapis-Poppins','https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,300|', array( 'tourismfl-basic-styles' ), '1.0' );
	wp_enqueue_style( 'fonts-googleapis-Poppins','https://fonts.googleapis.com/css?family=Lora:400,400i,700|Poppins:300,600', array( 'tourismfl-basic-styles' ), '1.0' );

	//Theme basic stylesheet. Includes normalize.css and some basic typography, elements and form styles.
	wp_enqueue_style( 'tourismfl-basic-styles', get_stylesheet_uri() );

	//Main stylesheet and javascript of theme.
	wp_enqueue_style( 'tif-icons', TEMPPATH . '/lib/fontello/css/fontello.css', array( 'tourismfl-basic-styles' ), '1.0' );

	wp_enqueue_style( 'tourismfl-styles', TEMPPATH . '/css/main.css', array( 'tourismfl-basic-styles', 'tif-icons' ), '1.0' );
	wp_enqueue_script( 'tourismfl-script', TEMPPATH . '/js/main.js', array( 'jquery' ), '1.0', true );

	//Localize main.js for AJAX
	wp_localize_script( 'tourismfl-script', 'TIF_Ajax', array(
			'ajaxurl'   => admin_url( 'admin-ajax.php' ),
			'tif_nonce' => wp_create_nonce( 'tif_ajax_nonce' )
		)
	);

	wp_enqueue_style( 'bootstrap-css', TEMPPATH . '/lib/bootstrap/css/bootstrap.css', array( 'tourismfl-styles' ), '3.3.5' );
	wp_enqueue_script( 'bootstrap-js', TEMPPATH . '/lib/bootstrap/js/bootstrap.js', array( 'jquery' ), '3.3.5', true );


	//Select2 CSS and JS.
	if(is_home() || is_front_page() || is_archive('attractions') || is_tax('activities') || is_singular('things-to-do') || is_404()) {
		wp_enqueue_style( 'select2-css', TEMPPATH . '/lib/select2/select2.css', array( 'tourismfl-styles' ), '1.0' );
		wp_enqueue_script( 'select2-js', TEMPPATH . '/lib/select2/select2.js', array( 'jquery' ), '4.0.3', true );
	}

	if(is_home() || is_front_page()) {
		wp_enqueue_style( 'owl.carousel-css', TEMPPATH . '/lib/owl-carousel/owl.carousel.css', array( 'tourismfl-styles' ), '1.0' );
		wp_enqueue_script( 'owl.carousel-script', TEMPPATH . '/lib/owl-carousel/owl.carousel.js', array( 'jquery' ), '1.0', true );

		wp_enqueue_style( 'tourismfl-home-styles', TEMPPATH . '/css/home.css', array( 'tourismfl-basic-styles', 'tif-icons' ), '1.0' );
		wp_enqueue_script( 'home-script', TEMPPATH . '/js/home.js', array( 'jquery' ), '1.0', true );
	}

	//single-attractions.php CSS and JS.
	if(is_singular('attractions')) {

		wp_enqueue_style( 'single-attractions', TEMPPATH . '/css/single-attractions2.css', array( 'tourismfl-styles' ), '1.0' );
		wp_enqueue_script( 'gmaps-api', 'https://maps.google.com/maps/api/js?key=AIzaSyAfu8e0nsAlXQ47GvufcThyI8jxmpD5oro', array(), null, true );
		wp_enqueue_script( 'gmap3', 'https://cdn.jsdelivr.net/gmap3/7.2.0/gmap3.min.js', array( 'jquery' ), null, true );
		//wp_enqueue_script( 'waypoint', TEMPPATH . '/lib/waypoint.js', array(), '1.8', true );
		wp_enqueue_script( 'single-attractions', TEMPPATH . '/js/single-attractions2.js', array( 'jquery', 'gmap3' ), '1.0', true );
//                wp_enqueue_style( 'owl.carousel-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.min.css', array( 'tourismfl-styles' ), '1.0' );
//		wp_enqueue_script( 'owl.carousel-script', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js', array( 'jquery' ), '1.0', true );
                wp_enqueue_style( 'flex-css', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css', array( 'tourismfl-styles' ), '1.0' );
		wp_enqueue_script( 'flex-script', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider.min.js', array( 'jquery' ), '1.0', true );
	}


	//single-cities.php CSS and JS.
	if(is_singular('things-to-do')) {
		wp_enqueue_style( 'single-things-to-do', TEMPPATH . '/css/single-things-to-do.css', array( 'tourismfl-styles' ), '1.0' );
		wp_enqueue_script( 'single-things-to-do', TEMPPATH . '/js/single-things-to-do.js', array( 'jquery' ), '1.0', true );
	}

	//single-attractions page CSS and JS.
	if(is_archive('things-to-do') || is_page_template( 'templates/attractions-search.php' )) {
		wp_enqueue_style( 'attractions-search-css', TEMPPATH . '/css/archive-attractions.css', array( 'tourismfl-styles' ), '1.0' );
	}

	//Contact page CSS and JS.
	if(is_page_template( 'templates/contact.php' )) {
		if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
        wpcf7_enqueue_scripts();
    	}
		if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
			wpcf7_enqueue_styles();
		}
		wp_enqueue_style( 'contact-css', TEMPPATH . '/css/contact.css', array( 'tourismfl-styles' ), '1.0' );
	}

	//Contact page CSS and JS.
	if(is_page( 'things-to-do' )) {
		wp_enqueue_style( 'page-things-to-do-css', 	TEMPPATH . '/css/page-things-to-do.css', array( 'tourismfl-styles' ), '1.0' );
	}

	//Blog page CSS and JS.
	if(is_home()) {

		//wp_enqueue_style(  'blog-pages-styles', 	TEMPPATH . '/css/blog-pages.css', 		 array( 'tourismfl-basic-styles', 'tif-icons' ), '1.0' );
				// Pull Masonry from the core of WordPress
		//wp_enqueue_script( 'masonry' );
		// wp_enqueue_script( 'masonry-script', 		TEMPPATH . '/lib/masonry.js', 	 		 array( 'jquery' ), '1.0', true );
		// wp_enqueue_script( 'isotope-script', 		TEMPPATH . '/lib/isotope-docs.min.js', 	 array( 'jquery' ), '1.0', true );
		// wp_enqueue_script( 'blog-pages-script', 	TEMPPATH . '/js/blog-pages.js', 		 array( 'jquery' ), '1.0', true );
		// wp_enqueue_script( 'loadmorepost-script', 	TEMPPATH . '/js/loadmorepost.js', 		 array( 'jquery' ), '1.0', true );

	}

	//Category and Tag page CSS and JS.
	if(is_home() || is_category() || is_tag()) {
		wp_enqueue_style( 'owl.carousel-2-css', 		TEMPPATH . '/lib/owl-carousel-2/assets/owl.carousel.css', array( 'tourismfl-styles' ), '1.0' );
		wp_enqueue_script( 'owl.carousel-2-script', 	TEMPPATH . '/lib/owl-carousel-2/owl.carousel.js', array( 'jquery' ), '1.0', true );

		wp_enqueue_style( 'single-styles', 		TEMPPATH . '/css/blog.css', array( 'tourismfl-basic-styles', 'tif-icons' ), '1.0' );
		wp_enqueue_script( 'blog-pages-script', 	TEMPPATH . '/js/blog-pages.js', 		 array( 'jquery' ), '1.0', true );
	}

	//Single blog post CSS and JS.
	if(is_singular('post') || is_attachment()) {
		wp_enqueue_style( 'single-styles', 		TEMPPATH . '/css/blog.css', array( 'tourismfl-basic-styles', 'tif-icons' ), '1.0' );
	}

	//Account page CSS and JS.
	if(is_page( 'account' )) {
		wp_enqueue_style( 'page-account-css', 	TEMPPATH . '/css/account.css', array( 'tourismfl-styles' ), '1.0' );
		wp_enqueue_script( 'page-account-js', TEMPPATH . '/js/account.js', array( 'jquery' ), '1.0', true );
	}
}
add_action( 'wp_enqueue_scripts', 'tourismfl_enqueues' );
