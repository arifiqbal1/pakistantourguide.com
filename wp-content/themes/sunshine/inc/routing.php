<?php

function tif_custom_rewrites()  {
	add_rewrite_rule(
		'^things-to-do/?$',
		'index.php?pagename=things-to-do',
		'top' );

		add_rewrite_rule(
			'^things-to-do/florida-keys/?$',
			'index.php?post_type=things-to-do&name=florida-keys',
			'top' );

		add_rewrite_rule(
			'^things-to-do/cocoa-beach/?$',
			'index.php?post_type=things-to-do&name=cocoa-beach',
			'top' );

		add_rewrite_rule(
			'^things-to-do/melbourne/?$',
			'index.php?post_type=things-to-do&name=melbourne',
			'top' );

		add_rewrite_rule(
			'^things-to-do/homestead/?$',
			'index.php?post_type=things-to-do&name=homestead',
			'top' );

		add_rewrite_rule(
			'^things-to-do/riviera-beach/?$',
			'index.php?post_type=things-to-do&name=riviera-beach',
			'top' );
}
add_action('init', 'tif_custom_rewrites', -1, 0);
