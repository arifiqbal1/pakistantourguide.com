<?php

function tif_accuracy_disclaimer( $atts ) {
  $a = shortcode_atts( array(
    'date' => current_time("m/d/Y")
  ), $atts );

  return '<p class="tif_accuracy_disclaimer">This information above was gathered by TourisminFlorida.com, and was believed to be accurate as of '.$a['date'].'</p>';
}
add_shortcode( 'accuracy_disclaimer', 'tif_accuracy_disclaimer' );
