<?php

require_once 'attraction.php';

class City {

  public $ID					= 0;
	public $name 			  = false;
	public $url 			  = false;
	public $description	= false;
	public $details			= false;
	public $image				= false;
  public $attractions = false;

  //Constructor
  public function __construct($ID) {
    $this->ID = $ID;
	}

  //Get city name (post title)
  public function get_name() {
    if(!$this->name) {
      $this->name = get_the_title($this->ID);
    }
    return $this->name;
  }

  //Get permalink
  public function get_url() {
    if(!$this->url) {
      $this->url = get_the_permalink($this->ID);
    }
    return $this->url;
  }

  //Get short description
  public function get_description() {
    if(!$this->description) {
      $this->description = get_field('short_description', $this->ID);
    }
    return $this->description;
  }

  //Get long description (content)
  public function get_details() {
    if(!$this->details) {
      $post_object = get_post( $this->ID );
      $this->details = apply_filters( 'the_content', $post_object->post_content);
    }
    return $this->details;
  }

  //Get featured image
  public function get_image($size='thumbnail') {
    if(!$this->image) {
      $attachment  = wp_get_attachment_image_src(get_post_thumbnail_id($this->ID ), $size);
      $this->image = $attachment[0];
    }
    return $this->image;
  }

  //Get city Attractions
  public function get_attractions() {
    if(!$this->attractions) {
      $this->attractions = array();

      $args  = array(
        'posts_per_page'	=> -1,
        'post_type'		=> 'attractions',
        'orderby'     => 'title',
      	'order'			  => 'asc',
        'meta_query'	=> array(
          'relation' => 'OR',
      		array(
      			'key'	  	  => 'city',
      			'value'	  	=>  $this->ID,
      			'compare' 	=> '=',
      		),
          array(
            'key'       => 'related_items',
            'value'     => '"' . $this->ID . '"',
            'compare'   => 'LIKE'
          )
      	)
      );
      $query = new WP_Query( $args );

      while($query->have_posts()) {
        $query->the_post();
        $attraction = new Attraction(get_the_ID());
        array_push($this->attractions, $attraction);
      }
      wp_reset_postdata();
    }
    return $this->attractions;
  }

  //get all cities static
  public static function get_all_cities() {
    $args  = array(
      'posts_per_page'	=> -1,
      'post_type'		=> 'things-to-do',
      'orderby'     => 'title',
      'order'			  => 'asc'
    );
    $query = new WP_Query( $args );
    $cities  = array();

    while($query->have_posts()) {
      $query->the_post();

      $city = new City(get_the_ID());
      $city->name  = get_the_title();
      $city->url   = get_the_permalink();

      array_push($cities, $city);
    }
    wp_reset_postdata();
    return $cities;
  }
}
