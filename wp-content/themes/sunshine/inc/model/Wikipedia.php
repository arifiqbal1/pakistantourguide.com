<?php

class Wikipedia {

  public static $_apiUrl = 'https://en.wikipedia.org/w/api.php?format=json&action=query';

  //Constructor
  public function __construct() {
	}

  public static function get_overview($postID, $refresh=true) {

    $wikipedia_handle = get_field('wikipedia_handle', $postID);

    $transient     = 'wiki_place'.$postID.'_overview';
    $transientTime =  604800;  //1 week
    $overview = trim(get_transient( $transient ));

    if(!$overview || $refresh) {
      $getParameters = "&prop=extracts&exintro=&explaintext=&titles=".$wikipedia_handle;
      $requestURL = Wikipedia::$_apiUrl . $getParameters;
      $jsonResult = file_get_contents($requestURL);
      $result     = json_decode($jsonResult);

      $overview   = trim(current((array)$result->query->pages)->extract);
      set_transient( $transient , $overview, $transientTime ); 
    }
    if($overview != '') {
      $overview   = "<p>".$overview."</p>";
      $overview   = str_replace("\n", "</p><p>", $overview);
    }

    return $overview;
  }
}

