<?php

class GooglePlaces {
  private static $apiKey = "";

  public static $_apiKey = 'AIzaSyDsQqucs35ZRPNEMDImedCT4vnetyXOJow';
  public static $_apiUrl = 'https://maps.googleapis.com/maps/api/place/';

  //Constructor
  public function __construct() {
	}

  public static function getPlace($placeID, $postID, $refresh=false) {
    $transient     = 'gplace_'.$postID.'_json';
    $transientTime =  604800;  //12 * HOUR_IN_SECONDS 12 hours
    $result = get_transient( $transient );
    $jsonResult = get_transient( $transient );
    $result     = json_decode($jsonResult);
    if(!$result->result || $refresh) {
      $getPlaceParameters = "details/json?placeid=" . $placeID . "&key=" . GooglePlaces::$_apiKey;
      $requestURL = GooglePlaces::$_apiUrl . $getPlaceParameters;
      $jsonResult = file_get_contents($requestURL);
      $result     = json_decode($jsonResult);
      set_transient( $transient , $jsonResult, $transientTime ); 
    }
    //var_dump($result);
    //die();
    return $result->result;
  }

  public static function getPlaceTimings($placeID, $postID) {
    $place = GooglePlaces::getPlace($placeID, $postID);
    return $place->opening_hours;
  }
}
