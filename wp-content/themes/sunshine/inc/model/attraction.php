<?php

require_once 'activity.php';
require_once 'city.php';
require_once 'city.php';
require_once 'GooglePlaces.php';
require_once 'Wikipedia.php';

class Attraction {

  public $ID					= 0;
  public $name 			  = false;
  public $url 			  = false;
  public $description	= false;
  public $details			= false;
  public $image				= false;
  public $category		= false;
  public $timings		  = false;
  public $GooglePlaceID = false;

  public $fulladdress = false;
  public $address     = false;
  public $city        = false;
  public $state       = "FL";
  public $zip         = false;

  public $longitude   = 0;
  public $latitude    = 0;

  public $GooglePlaceObject = false;
  private $GoogleAddressArr = false;

  //Wikipedia
  public $wikiOverview    = "";

  public $refresh    = false;

  //Constructor
  public function __construct($ID) {
    $this->ID = $ID;
	}

  //Get attraction name (post title)
  public function get_name() {
    if(!$this->name) {
      $this->name = get_the_title($this->ID);
    }
    return $this->name;
  }

  //Get permalink
  public function get_url() {
    if(!$this->url) {
      $this->url = get_the_permalink($this->ID);
    }
    return $this->url;
  }

  //Get short description
  public function get_description() {
    if(!$this->description) {
      $this->description = get_field('short_description', $this->ID);
    }
    return $this->description;
  }

  //Get long description (content)
  public function get_details() {
    if(!$this->details) {
      $post_object = get_post( $this->ID );
      $this->details = apply_filters( 'the_content', $post_object->post_content);
    }
    return $this->details;
  }

  //Get featured image
  public function get_image($size='thumbnail') {
    if(!$this->image) {
      $attachment  = wp_get_attachment_image_src(get_post_thumbnail_id($this->ID ), $size);
      $this->image = $attachment[0];
    }
    return $this->image;
  }

  //Get attraction category
  public function get_category() {
    if(!$this->category) {
      $activities = wp_get_post_terms($this->ID, 'activities');
      if(count($activities) > 0) {
        $this->category = new Activity( $activities[0]->term_id);
      }
    }
    return $this->category;
  }

  //Get GooglePlaceID
  public function get_GooglePlaceID() {
    if(!$this->GooglePlaceID) {
      //$this->GooglePlaceID = get_the_title('GooglePlaceID', $this->ID);
      $this->GooglePlaceID = get_field('google_place_id', $this->ID);
    }
    return $this->GooglePlaceID;
  }

  //Get GooglePlace
  public function get_GooglePlaceObject() {

    if(!$this->GooglePlaceObject) {
      $this->GooglePlaceObject = GooglePlaces::getPlace($this->get_GooglePlaceID(), $this->ID);
    }
    return $this->GooglePlaceObject;
  }

  //Get GooglePlaceID
  public function refresh_GooglePlaceObject() {
    if(!$this->GooglePlaceID) {
      $this->GooglePlaceObject = GooglePlaces::getPlace($this->get_GooglePlaceID(), $this->ID, true);
    }
    return $this->GooglePlaceID;
  }

/*
** Address Stuff
  --------------------------*/
  private function getGoogleAddressArr() {

    if($this->GoogleAddressArr) {
      return $this->GoogleAddressArr;
    }
    else {

      $location = array();

      if(!$this->GooglePlaceObject) {
        $this->GooglePlaceObject = GooglePlaces::getPlace($this->get_GooglePlaceID(), $this->ID);
      }

      if($this->GooglePlaceObject->address_components) {
        foreach ($this->GooglePlaceObject->address_components as $component) {
          switch ($component->types) {
            case in_array('street_number', $component->types):
              $location['street_number'] = $component->long_name;
              break;
            case in_array('route', $component->types):
              $location['street'] = $component->long_name;
              break;
            case in_array('sublocality', $component->types):
              $location['sublocality'] = $component->long_name;
              break;
            case in_array('locality', $component->types):
              $location['locality'] = $component->long_name;
              break;
            case in_array('administrative_area_level_2', $component->types):
              $location['admin_2'] = $component->types;
              break;
            case in_array('administrative_area_level_1', $component->types):
              $location['admin_1'] = $component->long_name;
              break;
            case in_array('postal_code', $component->types):
              $location['postal_code'] = $component->long_name;
              break;
            case in_array('country', $component->types):
              $location['country'] = $component->long_name;
              break;
          }
        }
      }

      $this->GoogleAddressArr = $location;
      return $this->GoogleAddressArr;
    }
  }

  //Get attraction fulladdress
  public function get_fulladdress() {
   if(!$this->fulladdress) {
      $this->address = $this->get_address();
      $this->zip     = $this->get_zip();
      $this->city     = $this->get_city();
      $line_break = "<br />";
      if(strlen($this->address) > 23) {
        $line_break = "";
      }


      if(!$this->address) {
        if(!$this->city) 
          $this->fulladdress = "Florida";
        else 
          $this->fulladdress = "{$this->city}, Florida";
      } else {
        $this->fulladdress = "{$this->address}, {$line_break}{$this->city}, {$this->state} {$this->zip}";
      }
    }
    return $this->fulladdress;
  }

  //Get street address
  public function get_address() {
    if(!$this->address) {
      $addressArr = $this->getGoogleAddressArr();

      $this->address = "";
      if(isset($addressArr['street_number'])) {
        $this->address .= $addressArr['street_number'] . " ";
      }

      if(isset($addressArr['street'])) {
        $this->address .= $addressArr['street'];
      }
    }
    return $this->address;
  }

  //Get city
  public function get_city() {
    if(!$this->city) {
      $addressArr = $this->getGoogleAddressArr();

      if(isset($addressArr['locality'])) {
        $this->city = $addressArr['locality'];
      }
      else {
        $this->city = "";
      }
    }
    return $this->city;
  }

  //Get state
  public function get_state() {
    return "FL";
  }

  //Get zip code
  public function get_zip() {
    if(!$this->zip) {
      $addressArr = $this->getGoogleAddressArr();

      if(isset($addressArr['postal_code'])) {
        $this->zip = $addressArr['postal_code'];
      }
      else {
        $this->zip = "";
      }
    }
    return $this->zip;
  }

  //get longitude
  public function get_longitude() {

    $place_longitude = get_field('longitude', $this->ID);
    if($place_longitude && !$refresh) {
      $this->longitude = $place_longitude;
      return $this->longitude;
    }

    if(!$this->longitude) {
      $this->longitude = $this->get_GooglePlaceObject()->geometry->location->lng;
      update_field('longitude', $this->longitude, $this->ID);
    }
    return $this->longitude;
  }

  //get latitude
  public function get_latitude() {
    $place_latitude = get_field('latitude', $this->ID);
    if($place_latitude && !$refresh) {
      $this->latitude = $place_latitude;
      return $this->latitude;
    }

    if(!$this->latitude) {
      $this->latitude = $this->get_GooglePlaceObject()->geometry->location->lat;
      update_field('latitude', $this->latitude, $this->ID);
    }
    return $this->latitude;
  }

/*
** Wikipedia Stuff
  --------------------------*/
  //Get Phone
  public function get_wiki_overview() {

    if(!$this->wikiOverview) {
      $this->wikiOverview = Wikipedia::get_overview($this->ID, $refresh);
    }
    return $this->wikiOverview;
  }

/*
** Ohter info
  --------------------------*/

  //Get Phone
  public function get_phone() {

    if(!$this->GooglePlaceObject) {
      $this->GooglePlaceObject = GooglePlaces::getPlace($this->get_GooglePlaceID(), $this->ID);
    }
    return $this->GooglePlaceObject->formatted_phone_number;
  }

  //Get Website
  public function get_website() {

    if(!$this->GooglePlaceObject) {
      $this->GooglePlaceObject = GooglePlaces::getPlace($this->get_GooglePlaceID(), $this->ID);
    }
    return $this->GooglePlaceObject->website;
  }

  //get location timing
  public function get_timings($place_id=false) {
    if(!$this->timings) {
      $this->timings = $this->get_GooglePlaceObject()->opening_hours;
    }
    return $this->timings;
  }

  //find attractions query
  public static function find_attractions_query($cityID, $attractionTypeID) {

    $args  = array();

    $tax_query = array();
    if($attractionTypeID && $attractionTypeID  != 0) {
      $tax_query = array(
    		array(
    			'taxonomy' => 'activities',
    			'field'    => 'term_id',
          'terms'    => $attractionTypeID,
    			// 'operator' => '='
    		),
    	);

      $args['tax_query']  = $tax_query;
    }

    $meta_query = array();
    if($cityID && $cityID !=0) {
      $meta_query = array(
    		array(
    			'key'     => 'city',
    			'value'   => $cityID,
    			'compare' => '=',
    		),
    	);

      $args['meta_query']  = $meta_query;
    }

    return $args;
  }

  //Get attraction by city and attraction type
  public static function find_attractions($cityID=0, $attractionTypeID=0, $perPage=-1, $orderBy='title', $order='asc') {

    $taxMetaArgs = Attraction::find_attractions_query($cityID, $attractionTypeID);

    $args  = array(
      'posts_per_page'	=> $perPage,
      'post_type'		=> 'attractions',
      'orderby'     => $orderBy,
      'order'			  => $order,
    );

    $args = array_merge( $args, $taxMetaArgs );

    $query = new WP_Query( $args );
    $attractions  = array();

    while($query->have_posts()) {
      $query->the_post();

      $attraction = new Attraction(get_the_ID());
      array_push($attractions, $attraction);
    }
    wp_reset_postdata();
    return $attractions;
  }
}
