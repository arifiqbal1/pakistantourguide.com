<?php

class Activity {

  public $ID					= 0;
  public $name 			  = false;
  public $url 			  = false;
  public $description	= false;
  public $image				= false;

  //Constructor
  public function __construct($ID) {
    $this->ID = $ID;
    $this->get_activity();
	}

  //Get attraction name (post title)
  public function get_name() {
    if(!$this->name) {
      $this->get_activity();
    }
    return $this->name;
  }

  //Get permalink
  public function get_url() {
    if(!$this->url) {
      $this->get_activity();
    }
    return $this->url;
  }

  //Get short description
  public function get_description() {
    if(!$this->description) {
      $this->description = term_description($this->ID, 'activities');
    }
    return $this->description;
  }

  //Get featured image
  public function get_image() {
    if(!$this->image) {
      $this->image = get_field('attraction_category_image', 'activities_'.$this->ID);
    }
    return $this->image;
  }

  private function get_activity() {
    $term = get_term($this->ID);
    if($term) {
      $this->name = $term->name;
      $this->url  =  get_term_link($term);
    }
  }

  public static function get_all_activities() {
    $args = array(
      'taxonomy' => 'activities',
      'orderby'  => 'title',
    );
    $terms      = get_terms($args);
    $activities = array();
    foreach($terms as $term) {
      $activity = new Activity($term->term_id);
      array_push($activities, $activity);
    }
    wp_reset_postdata();
    return $activities;
  }
}
