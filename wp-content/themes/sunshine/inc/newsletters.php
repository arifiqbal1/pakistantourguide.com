<?php

require_once TEMPDIR . '/lib/mailchimp/MCAPI.class.php';

add_action( 'wp_ajax_tifNewsletterSubscribe', 'tifNewsletterSubscribe' );
add_action( 'wp_ajax_nopriv_tifNewsletterSubscribe', 'tifNewsletterSubscribe' );

function tifNewsletterSubscribe()
{
	$nonce = $_POST['security'];
	if ( !wp_verify_nonce( $nonce, 'tif_ajax_nonce' ) || !isset($_POST['data']) ) 
	{
		die ( 'Busted!' );
	}

	$data 	= $_POST['data'];
	$email 	= filter_var($data['email'], FILTER_SANITIZE_STRING);

	$apikey = '9a3b2a530d6e3fa179ab0b99999080ca-us8';	//API Key
	$listId = '87b4827de3'; 							//List Id
	$apiUrl = 'http://api.mailchimp.com/1.3/'; 			//Just used in xml-rpc

	$api 	= new MCAPI($apikey);
	$retval = $api->listSubscribe( $listId, $email, $merge_vars );

	if ($api->errorCode)
	{
		//echo "\tCode=".$api->errorCode."\n";
		//echo "\tMsg=".$api->errorMessage."\n";
		echo "2";
	} 
	else 
	{
	    echo "1";
	}

	exit;
}