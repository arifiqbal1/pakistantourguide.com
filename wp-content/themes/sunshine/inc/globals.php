<?php
define('ABSPATH', dirname(__FILE__).'/');
define('TEMPPATH' , get_bloginfo('stylesheet_directory'));
define('IMAGES', TEMPPATH.'/images');
define('URL', get_bloginfo('url'));
define('TEMPDIR', get_stylesheet_directory());
define('SHOWADS', false);

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails', array( 'post', 'page', 'things-to-do', 'attractions' ) );

update_option( 'medium_size_w', 273 );
update_option( 'medium_size_h', 146 );
update_option( 'medium_crop', 1 );

add_image_size("blogpost-thumb", 370, 208, array( 'center', 'center' ));


add_filter( 'xmlrpc_enabled', '__return_false' );
// Hide xmlrpc.php in HTTP response headers
add_filter( 'wp_headers', function( $headers ) {
    unset( $headers[ 'X-Pingback' ] );
    return $headers;
});
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
remove_action( 'wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_generator');


function getFeaturedImageURL($post_id) {
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full');
	return $image[0];
}

function convert_state($key) {
    $a2s = array(
        'Alabama'=>'AL',
        'Alaska'=>'AK',
        'Arizona'=>'AZ',
        'Arkansas'=>'AR',
        'California'=>'CA',
        'Colorado'=>'CO',
        'Connecticut'=>'CT',
        'Delaware'=>'DE',
        'Florida'=>'FL',
        'Georgia'=>'GA',
        'Hawaii'=>'HI',
        'Idaho'=>'ID',
        'Illinois'=>'IL',
        'Indiana'=>'IN',
        'Iowa'=>'IA',
        'Kansas'=>'KS',
        'Kentucky'=>'KY',
        'Louisiana'=>'LA',
        'Maine'=>'ME',
        'Maryland'=>'MD',
        'Massachusetts'=>'MA',
        'Michigan'=>'MI',
        'Minnesota'=>'MN',
        'Mississippi'=>'MS',
        'Missouri'=>'MO',
        'Montana'=>'MT',
        'Nebraska'=>'NE',
        'Nevada'=>'NV',
        'New Hampshire'=>'NH',
        'New Jersey'=>'NJ',
        'New Mexico'=>'NM',
        'New York'=>'NY',
        'North Carolina'=>'NC',
        'North Dakota'=>'ND',
        'Ohio'=>'OH',
        'Oklahoma'=>'OK',
        'Oregon'=>'OR',
        'Pennsylvania'=>'PA',
        'Rhode Island'=>'RI',
        'South Carolina'=>'SC',
        'South Dakota'=>'SD',
        'Tennessee'=>'TN',
        'Texas'=>'TX',
        'Utah'=>'UT',
        'Vermont'=>'VT',
        'Virginia'=>'VA',
        'Washington'=>'WA',
        'West Virginia'=>'WV',
        'Wisconsin'=>'WI',
        'Wyoming'=>'WY'
    );

    if(strlen($key) == 2){
        $state = $key;
    }else{
        $state = $a2s[$key];
    }
    return $state;
}


//Get featured image URL
function get_featured_image($post_id, $size='full') {
	$attachment  = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), $size);
	return $attachment[0];
}

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_get_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    return $count;
}


function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

function short_description_words_limit( $num_words = 20, $post_content = null )
{
    $ending = '...';
    $excerpt = wp_trim_words( $post_content, $num_words, $ending );
    return $excerpt;
}
function title_words_limit( $num_words = 5, $post_title = null )
{
    $ending = '..';
    $title = wp_trim_words( $post_title, $num_words, $ending );
    return $title;
}

function get_url_domain($input) {
   // in case scheme relative URI is passed, e.g., //www.google.com/
    $input = trim($input, '/');

    // If scheme not included, prepend it
    if (!preg_match('#^http(s)?://#', $input)) {
        $input = 'http://' . $input;
    }

    $urlParts = parse_url($input);

    // remove www
    $domain = preg_replace('/^www\./', '', $urlParts['host']);

    return $domain;
}

//Get day's name
function get_day_name($number) {
    $daysArr = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
    return $daysArr[$number];
}

//Get header tracking scripts
function header_tracking_scripts() {
    if(!is_user_logged_in()) {
        ?>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-77767153-1', 'auto');
          ga('send', 'pageview');
        </script>

        <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', '4d2abe8ed185049004f1e7be080cc8aef0b82156');
        </script>
        <?php
    }
}















