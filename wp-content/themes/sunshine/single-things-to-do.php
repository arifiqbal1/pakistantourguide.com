<?php

get_header();

while ( have_posts() ) : the_post();
$city = new City(get_the_ID());

$showReviews = false;

?>

<section itemscope itemtype="http://schema.org/City">
	<section class="hero-section" style="background-image: url(<?= $city->get_image('full') ?>)">
		<div class="container">
			<div class="hero-content">
				<div class="hero-content-outer">
					<div class="hero-content-inner">
						<h1>Things to do in <span itemprop="name"><?= $city->get_name() ?></span></h1>
						<p itemprop="description"><?= $city->get_description() ?></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="breadcrumb-section">
		<div class="container">
			<ol itemscope itemtype="http://schema.org/BreadcrumbList" class="clearfix">
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a href="<?= URL ?>" itemprop="item">
						<span itemprop="name">Home</span>
					</a>
				</li>
				<li>
					<span>Things to do in <?= $city->get_name() ?></span>
				</li>
			</ol>
		</div>
	</section>

	<?php //get_attraction_search() ?>

	<?php
		$attractions = $city->get_attractions();
	?>
	<?php if(count($attractions) > 0): ?>
	<section class="city-attractions tif-section">
		<div class="container">
			<h2>Tourist Attractions in <?= $city->get_name() ?></h2>

			<div class="city-attractions-cols clearfix">
				<?php foreach($attractions as $attraction): ?>
					<?php attraction_grid_item($attraction) ?>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php
	$blog_query = new WP_Query( array( 
					'post_type' => 'post',
					'posts_per_page' => -1, 
					'meta_query' => array(
				        array(
				            'key' => 'article_relations', // name of custom field
				            'value' => '"' . get_the_ID() . '"',
				            'compare' => 'LIKE'
				        )
				    )
				)); 
	?>
	<?php if ($blog_query->have_posts()): ?>
	<section class="featured-blog-section tif-section">
		<div class="container">
			<h2>Articles about <?= $city->get_name() ?></h2>

			<div class="featured-blog-cols clearfix">
				<?php while($blog_query->have_posts()): $blog_query->the_post(); ?>
				<?php blog_grid_item() ?>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>

	<section class="city-overview tif-section">
		<div class="container">
			<h2>About <?= $city->get_name() ?></h2>
			<div class="textcontent-section">
				<?php the_content() ?>
			</div>
		</div>
	</section>

</section>

<?php endwhile; ?>

<?php get_footer(); ?>
