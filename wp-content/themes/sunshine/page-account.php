<?php

get_header("nonav");
while ( have_posts() ) : the_post();
?>

<section class="hero-section myaccount-hero" style="background-image: url(<?php echo get_featured_image(get_the_ID()) ?>)">
  <div class="container">
    
  </div>
</section>

<section id="account-modal" class="account-modal modal fadeIn" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="account-modal-wrapper">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">

        </div> 
      </div>
    </div>
  </div>
</section>

<!-- <div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Modal title</h4>
</div> -->

<!-- <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button>
    </div> -->

<?php endwhile; ?>
<?php get_footer("nonav"); ?>
