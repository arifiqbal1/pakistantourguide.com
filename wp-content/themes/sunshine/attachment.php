<?php
  $amp = false; 
  if(isset($_GET["amp"])) $amp = true;
?>

<?php
  get_header('without-hero');
?>

<?php get_cats_menu(); ?>

<?php if ( function_exists('yoast_breadcrumb') ) : ?>
<section class="breadcrumb-section">
  <div class="container">
    
    <ol class="clearfix">
      <li>&nbsp;</li>
    </ol>
    
  </div>
</section>
<?php endif; ?>

<section class="blog-section">
  <div class="container clearfix">
    <div class="blog-content-area">
      <?php while ( have_posts() ) : the_post(); ?>
      <article class="single-post-section">
        <h1 itemprop="name" class="post-title"><?php the_title() ?></h1>
        <?php if ( has_excerpt() ) : ?>
        <p class="post-overview"><?php the_excerpt(); ?></p>
        <?php endif; ?>

        <div class="entry-attachment">
			<?php $image_size = apply_filters( 'wporg_attachment_size', 'large' ); 
            echo wp_get_attachment_image( get_the_ID(), $image_size ); ?>
		</div><!-- .entry-attachment -->
		
        <div class="post-content content">
          <?php the_content() ?>
        </div>
      </article>
      <?php endwhile; ?>
    </div>
    <?php get_sidebar(); ?>
  </div>
</section>

<?php get_footer(); ?>
