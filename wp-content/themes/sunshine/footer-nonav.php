<section class="footer-nonav">
	<section class="footer-widgets">
		<div class="footer-widgets-border">
			<div class="footer-border-inner"></div>
		</div>
	</section>

	<footer class="tif-footer">
		<div class="container">&copy; 2016 TourismInFlorida.com</div>
	</footer>
</section>

<?php wp_footer(); ?>
</body>
</html>
