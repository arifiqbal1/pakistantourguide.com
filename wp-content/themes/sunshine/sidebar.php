<aside class="blog-sidebar">

	<div class="blog-widget">
		<h3>Recent Popular</h3>
		<ul class="sidebar-posts clearfix">
			<?php
				$recentPostsArgs = array (
					'posts_per_page' => 10,
					'orderby' => 'post_date',
					'order' => 'DESC'
				);
				$recentPosts = new WP_Query($recentPostsArgs);
			?>
			<?php while($recentPosts->have_posts()): $recentPosts->the_post(); ?>
			<li><a href="<?php the_permalink() ?>" class="clearfix">
				<?php if (has_post_thumbnail()) : ?>
          <?php the_post_thumbnail('thumbnail'); ?>
        <?php endif; ?>
				<span><?php the_title() ?></span>
			</a></li>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</ul>
	</div>

</aside>
