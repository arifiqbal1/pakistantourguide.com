<?php get_header(); ?>

<section class="hero-section" style="background-image:url(<?php echo IMAGES ?>/templates/404.jpg)">
	<div class="container">
		<div class="hero-content">
			<div class="hero-content-outer">
				<div class="hero-content-inner">
					<h1>Oops! Page Not Found.</h1>
					<!-- <p>We bet you will love it!</p> -->
          <div class="hero-cta">
            <a href="<?php echo URL ?>">Back To Home</a>
          </div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_attraction_search() ?>

<?php get_footer(); ?>
